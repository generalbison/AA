"""ROOT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from members import urls as members_urls
from api import urls as api_urls
from import_app import urls as import_app_urls
from datenschleuder import urls as datenschleuder_urls
from other import urls as other_urls
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^$', include(other_urls)), #other_views.index
    url(r'^admin/', include(admin.site.urls)),
    url(r'^members/', include(members_urls)),
    url(r'^api/', include(api_urls)),
    url(r'^import_app/', include(import_app_urls)),
    url(r'^datenschleuder/', include(datenschleuder_urls)),
]

urlpatterns += staticfiles_urlpatterns()
