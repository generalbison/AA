
def i_request(request):
    # Keeping type QueryDict for convenient usage
    i_request_cache = request.GET.copy()
    i_request_cache.update(request.POST)
    return i_request_cache


def get_fee_monthly(annual_fee):
    monthly_fee = float(annual_fee) / 12.0
    return monthly_fee


def get_fee_daily(annual_fee):
    daily_fee = float(annual_fee) / 365.0
    return daily_fee


def diff_dates(date1, date2):
    return abs(date2-date1).days


def str2bool(string):
    if string == 'true':
        return True
    else:
        return False
