from django.conf import settings
from django.shortcuts import render


def index(request):
    hasDatenschleuder = False
    if "datenschleuder" in settings.INSTALLED_APPS:
        hasDatenschleuder = True

    return render(request, "index_other.html", {'hasDatenschleuder':hasDatenschleuder})
