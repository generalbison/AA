from django.core.paginator import Paginator,EmptyPage, PageNotAnInteger

from django.shortcuts import render
from django.http import JsonResponse

from members.models import Member, Erfa, EmailAddress
from django import forms
from datetime import datetime
from import_app.forms_import import BankingImportForm
from django.template import RequestContext
import io, csv
import import_app.csv_parser as csv_parser
from django.db.models import Max
from django.forms import Textarea, NumberInput

from django.forms import modelformset_factory
from django.shortcuts import render_to_response, redirect
from .models import Transaction, DuplicateBankingWarning, NegativeCreditError


import logging
logging.getLogger(__name__)
logging.getLogger().setLevel(logging.INFO)


# Create your views here.
def run_import_banking(request):
    # Handle file upload
    uploadedFile = ''
    errors = []
    imported = []
    context = {}
    if request.method == 'POST':
#        form = DocumentForm(request.POST, request.FILES)
        form = BankingImportForm(request.POST, request.FILES)
        if form.is_valid():
            uploadedFile = request.FILES['docfile']
            data = uploadedFile.read().decode("iso-8859-1")
            f = io.StringIO(data)
            reader = csv.reader(f, delimiter=';')
            #headerRow = []
            #headerRowKey = {}
            #rowCount = 0
#            max_chaos_nr = Member.objects.all().aggregate(Max('chaos_number'))['chaos_number__max'] # todo, confirm removal
#            logging.info("Max assignes chaos nr is {}".format(max_chaos_nr))
            duplicate_warnings = []
            for row in reader:
                logging.debug("reading row: {}".format(row))
                if len(row) == 8 and not row[0].startswith('Buchungstag'):
                    new_transaction = csv_parser.TransactionReader(row)
                    new_tr_entry = Transaction()
                    try:
                        new_tr_entry.add_transaction(new_transaction)
                    except DuplicateBankingWarning:
                        duplicate_warnings.append('Duplicated entry not added: {}'.format(new_tr_entry.string_in))
                    except NegativeCreditError:
                        logging.info("Outgoing transaction skipped")
            if len(duplicate_warnings) > 0:
                context["warnings"] = duplicate_warnings
            context["message"] = "uploaded successfully"


    form = BankingImportForm() # A empty, unbound form
    #form = DocumentForm() # A empty, unbound form
    context.update({ 'form': form, 'uploadedFile': uploadedFile, 'errors': errors, 'imported': imported })
    logging.info("context is {}".format(context))
    response = render_to_response(
            'import_banking_csv.html',
            context,
            context_instance=RequestContext(request)
        )
    return response


def execute_transactions(request):
    context = {}
    template = 'execute_transactions.html' #todo

    context["showme"] = "Show all the transactions"
    context["message"] = "A message"
    if request.method == "POST":
        try:
            pending_transactions = Transaction.objects.filter(status=Transaction.STATUS_MATCHED_CHAOS_NR)
            for transaction in pending_transactions:
                transaction.execute_transaction()
            context["message"] = str(len(pending_transactions)) + "completed successfully"
        except:
            context["message"] = "something went wrong!"
    context["transactions"] = Transaction.objects.filter(status__in=[Transaction.STATUS_MATCHED_CHAOS_NR, Transaction.STATUS_UNKNOWN_CHAOS_NR])
    #print(context['transactions'])
    response = render_to_response(template, context, context_instance=RequestContext(request))
    return response
#    return render(request, template, context)

def manage_transactions_failed(request):
    """ show and manage failed transactions"""
    TransactionFormSet = modelformset_factory(Transaction, fields=('chaos_number','status'), extra=0)
    logging.info("manage_transactions: TransactionFormset".format(TransactionFormSet))
    if request.method == 'POST':
        formset = TransactionFormSet(request.POST, request.FILES)
        filteredformset = TransactionFormSet()
        if formset.is_valid():
            formset.save()
            logging.info("TMP {}".format(formset))
            return render(template_name="manage_transations_confirm.html",
                                      context={"formset": formset,
                                               "status_matching": Transaction.STATUS_MATCHED_CHAOS_NR,
                                               "status_erroneous": Transaction.STATUS_FAILED,
                                               }, request=request)

    else:
        # pagination goes here
        qs=Transaction.objects.filter(status = Transaction.STATUS_FAILED)
        paginator = Paginator(qs, 10)
        page = request.GET.get('page')
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        page_query = qs.filter(id__in=[object.id for object in objects])
        formset = TransactionFormSet(queryset=page_query)
        context = {'objects': objects, 'formset': formset, "status_matching": Transaction.STATUS_MATCHED_CHAOS_NR}
        return render_to_response('manage_transations.html', context,
                        context_instance=RequestContext(request))


def manage_transactions(request):
    TransactionFormSet = modelformset_factory(Transaction, fields=('chaos_number', 'status'), extra=0)
    logging.info("manage_transactions: TransactionFormset".format(TransactionFormSet))
    if request.method == 'POST':
        formset = TransactionFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            logging.info("TMP {}".format(formset))
            return render(template_name="manage_transations_confirm.html",
                          context={"formset": formset,
                                   "status_matching": Transaction.STATUS_MATCHED_CHAOS_NR,
                                   "status_erroneous": Transaction.STATUS_UNKNOWN_CHAOS_NR,
                                   }, request=request)

    # pagination goes here
    qs = Transaction.objects.filter(status=Transaction.STATUS_UNKNOWN_CHAOS_NR)\
         | Transaction.objects.filter(status=Transaction.STATUS_MATCHED_CHAOS_NR)
    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    page_query = qs.filter(id__in=[obj.id for obj in objects])
    formset = TransactionFormSet(queryset=page_query)
    context = {'objects': objects, 'formset': formset, "status_matching": Transaction.STATUS_MATCHED_CHAOS_NR}
    return render_to_response('manage_transations.html', context, context_instance=RequestContext(request))

    #return render(template_name="manage_transations.html", context={
    #    "formset": formset,
    #    "status_matching": Transaction.STATUS_MATCHED_CHAOS_NR,
    #}, request=request)


def manage_transactions_confirm(request):
    TransactionFormSet = modelformset_factory(Transaction, fields=('chaos_number','status'), extra=0)
    if request.method == 'POST':
        formset = TransactionFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            logging.info("TMP {}".format(formset))
            return render(template_name="manage_transations_confirm.html",
                                      context={"formset": formset}, request=request)

    else:


        return render(template_name="base.html", context={

    }, request=request)

    #if request.method == 'POST':
    #     # create a form instance and populate it with data from the request:
    #     form = request.POST
    #     # check whether it's valid:
    #     if form.is_valid():
    #         # process the data in form.cleaned_data as required
    #         # ...
    #         # redirect to a new URL:
    #
    #
    # return render_to_response("manage_transations_confirm.html",  {'form': form,  }, context_instance = RequestContext(request))
def manage_transactions_book(request):
    logging.warning("book")
    TransactionFormSet = modelformset_factory(Transaction, fields=('chaos_number','status'), extra=0)
    if request.method == 'POST':
        context={"formset": [],  # formset because that's what they've been called since import
                 "status_failed": Transaction.STATUS_FAILED,
                 "n_new_failed": 0,
                 "n_all_failed": 0,
                 "n_new_completed": 0
                 }
        formset = TransactionFormSet(request.POST, request.FILES)
        logging.debug("Booking {}".format(formset))
        for form in formset:
            logging.debug(form.instance)
            if form.instance.status == Transaction.STATUS_MATCHED_CHAOS_NR:
                form.instance.book()
                context["formset"].append(form)
                if form.instance.status == Transaction.STATUS_FAILED:
                    context["n_new_failed"] += 1
                elif form.instance.status == Transaction.STATUS_COMPLETED:
                    context["n_new_completed"] += 1

        context["n_all_failed"] = len(Transaction.objects.filter(status=Transaction.STATUS_FAILED))
        return render(template_name="success.html", context=context, request=request)


    else:
        return  render(template_name="base.html",
                                      context={}, request=request)


def member_lookup(request, chaosnr):
    logging.info("member_lookup called for {}".format(chaosnr))
    try:
        member = Member.objects.get(chaos_number=chaosnr)
        return JsonResponse({'member_first_name':
                             member.first_name,
                         'member_last_name':
                             member.last_name,
                         'chaosnr_asked': chaosnr,
                         'member_chaos_number':member.chaos_number,
                         'member_paid_until': member.fee_paid_until
                         })

    except Member.DoesNotExist:

        return JsonResponse({'member_first_name': None,
                            'member_last_name': None,
                         'chaosnr_asked': chaosnr,
                         'member_chaos_number': None,
                         'member_paid_until': None
                         })


