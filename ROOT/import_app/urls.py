from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^import_banking_csv/', views.run_import_banking, name='import_banking'),
    url(r'^execute_transactions/', views.execute_transactions, name='execute_transactions'),
    url(r'^manage_transactions/', views.manage_transactions, name='manage_transactions'),
    url(r'^manage_transactions_failed/', views.manage_transactions_failed, name='manage_transactions_failed'),
    url(r'^manage_transactions_confirm/', views.manage_transactions_confirm, name='manage_transactions_confirm'),
    url(r'^manage_transactions_book/', views.manage_transactions_book, name='manage_transactions_book'),
    url(r'^member_lookup/(\d+)', views.member_lookup, name='member_lookup'),
]