#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings
from import_app.csv_parser import TransactionReader
import json
import csv
import datetime
from members.models import Member, BalanceTransactionLog
from django.db import IntegrityError

#from simple_history.models import HistoricalRecords
import logging

logging.getLogger(__name__)
logging.getLogger().setLevel(logging.INFO)

# Create your models here.
class Transaction(models.Model):
    RATING_BAD = 64
    NO_CHAOS_NUMBER = -1111

    STATUS_MATCHED_CHAOS_NR = 'M'
    STATUS_UNKNOWN_CHAOS_NR = 'U'
    STATUS_COMPLETED = 'C'
    STATUS_IGNOREFOREVER = 'I'
    STATUS_FAILED = 'F'
    # code blaming: symbols on classes _pending [fixed]
    STATUS_OPTIONS = ((STATUS_MATCHED_CHAOS_NR, 'matched chaos nr'),
                      (STATUS_UNKNOWN_CHAOS_NR, 'unknown chaos nr'),
                      (STATUS_COMPLETED, 'completed'),
                      (STATUS_IGNOREFOREVER, 'ignore forever'),
                      (STATUS_FAILED, 'failed at booking'))
    string_in = models.CharField(max_length=1000, unique=True)
    booking_day = models.DateField(null=True)  # todo, why am I getting null?
    available_on = models.DateField(null=True)  # todo ditto
    payment_type = models.CharField(max_length=80)
    information = models.TextField()
    referenz = models.TextField()
    verwendungszweck= models.TextField()
    payer = models.CharField(max_length=80)
    payee = models.CharField(max_length=80)
    amount = models.IntegerField()
    balance = models.IntegerField()
    chaos_number = models.IntegerField()
    chaosnr_options = models.CharField(max_length=200, blank=True, null=True)
    rating = models.PositiveSmallIntegerField()
    status = models.CharField(max_length=1, choices=STATUS_OPTIONS)

    #history = HistoricalRecords()
    def handle_chaos_nr_error(self):
        self.rating = Transaction.RATING_BAD
        logging.warning("pre wipe chaos number {}".format(self.chaos_number))
        self.chaos_number = Transaction.NO_CHAOS_NUMBER
        if self.status == self.STATUS_MATCHED_CHAOS_NR:
            self.status = self.STATUS_UNKNOWN_CHAOS_NR


    def add_transaction(self, transaction_reader, threshold=15, max_chaos_nr=pow(2, 63)-1):
        # todo, check for addition of identical transactions (transaction ID? whole entry?)
        if not isinstance(transaction_reader, TransactionReader):
            raise ValueError()
        # code blaming: ._meta.get_fields() [fixed]
        transaction_fields = self._meta.get_fields()
        transaction_fields = [x.name for x in transaction_fields]
        shared_keys = set(transaction_fields) & set(transaction_reader.__dict__.keys())
        shared_keys.remove('chaos_number')
        if transaction_reader.amount <= 0:
            raise NegativeCreditError
        for key in shared_keys:
            setattr(self, key, getattr(transaction_reader, key))

        try:
            if int(transaction_reader.chaos_number[0]) <= max_chaos_nr:
                self.chaos_number = int(transaction_reader.chaos_number[0])
            else:
                self.handle_chaos_nr_error()
            # code blaming: jason field in postgress (third party) todo
            self.chaosnr_options = json.dumps(transaction_reader.chaos_number[1:])
        except ValueError as e:
            logging.warning("Value Error {}".format(e))
            self.handle_chaos_nr_error()
        except TypeError as e:
            logging.warning("Type Error {}".format(e))
            self.handle_chaos_nr_error()
        except IndexError as e:
            logging.warning("Index Error {}".format(e))
            self.handle_chaos_nr_error()

        if transaction_reader.donation:
            self.status = self.STATUS_IGNOREFOREVER
        elif self.rating < threshold:
            self.status = self.STATUS_MATCHED_CHAOS_NR
        else:
            self.status = self.STATUS_UNKNOWN_CHAOS_NR
        try:
            self.save()
        except IntegrityError as e:
            logging.warning('db integrity constraint failed, unique transaction for: {}\n{}?'.format(self.chaos_number, e))
            raise DuplicateBankingWarning


    def __str__(self):
        return str(self.chaos_number)

    def book(self):
        if not self.status == Transaction.STATUS_MATCHED_CHAOS_NR:
            raise TransactionNotPendingError
        try:
            mem_match = Member.objects.get(pk=self.chaos_number)
            mem_match.increase_balance_by(increase_by=self.amount,
                                          reason=BalanceTransactionLog.IMPORT_BANKING,
                                          comment=self.information)
            self.status = Transaction.STATUS_COMPLETED
        except Exception as e:
            self.status = Transaction.STATUS_FAILED
            logging.warning(e)
        self.save()


    def execute_transaction(self):
        # TODO: figure out if this method is entirely outdated/dead or still contains useful code, then cleanup
        # todo get prices from non-dummy location
        if self.status is not self.STATUS_MATCHED_CHAOS_NR:
            raise TransactionNotPendingError

        prices = {"reduced": settings.FEE_REDUCED, "normal": settings.FEE, "supporter": settings.FEE + 1, "registration": settings.FEE_ENTRY}  # todo supporter fee?
        amount = self.amount
        member = Member.objects.get(chaos_number=self.chaos_number)
        # pay registration
        if not member.fee_registration_paid and amount >= prices["registration"]:
            member.fee_registration_paid = True
            amount -= prices["registration"]
        # get applicable price
        if member.membership_reduced:
            yearly_price = prices["reduced"]
        elif member.membership_supporter:
            yearly_price = prices["supporter"]
        else:
            yearly_price = prices["normal"]
        # change balance to time paid
        years_paid = amount // yearly_price
        # todo pay first year, save remainder as some sort of BALANCE!!!
        # todo [dummy section until account/payment is decided on]
        previous_year = member.fee_paid_until.year
        member.fee_paid_until = member.fee_paid_until.replace(year=previous_year + years_paid)
        member.fee_last_paid = datetime.date.today()
        # [end dummy section]
        self.status = self.STATUS_COMPLETED
        # fee_last_paid
        # fee_paid_until
        # fee_registration_paid
        # chaos nr
        member.save()
        self.save()

    def get_expected_member(self):
        mem = Member.objects.get(pk=self.chaos_number)
        return mem


class TransactionNotPendingError(Exception):
    pass


class DuplicateBankingWarning(Warning):
    pass

class NegativeCreditError(Exception):
    pass

# todo discard 'Spende' & fee transactions that cannot be resolved

#todo Exception on Doppelmitgliedschaft and individual payment
#todo dont accept payment when membership end date set


def pseudotest():
#    csv_in = open("../testdata/anonymisierte_zahlungen.csv").readlines()
    csv_in = open("../testdata/PB_Umsatzauskunft_KtoNr0599090201_01-08-2015_2007.csv", encoding="iso-8859-1").readlines()
#    csv_in = csv.reader(csv_in)
    csv_in = csv.reader(csv_in, delimiter=';')
    for item in csv_in:
        print("---")
        if len(item) == 8:
            tr = TransactionReader(item)
            if tr.income:
                try:
                    new_tr = Transaction()
                    new_tr.add_transaction(tr)
                except Exception as e:
                    print(e)



# todo import app:
# name anzeigen
# lower confidence at least for present year
# pagination!!

# pending for every number after a key
# lower confidence for back2back numbers e.g. chaos nr. 123 4
# vergleich mit datenbank before manual checking
# comment field zu 1024
# spende raus/third color?
# will be paid until
# status ausblenden (failed/completed)
# umnennen erroneous -> unknown, pending -> match
# clean up debug output
# linking in with navigation
# failed transactions wieder anzeigen (by 'OK')

