import csv
from .csv_parser import TransactionReader
from .models import Transaction
import django
import logging

from django.test import TestCase

django.setup()

logging.getLogger(__name__)
logging.getLogger().setLevel(logging.DEBUG)
def line_from_csv(line):
    with open('import_app/tests.csv') as f:
        csvin = csv.reader(f, delimiter=';')
        csvin = list(csvin)
    return csvin[line]

def make_transaction_reader(line):
    line_split = line_from_csv(line)
    tr = TransactionReader(line_split)
    return tr

def make_transaction(line):
    tr = make_transaction_reader(line)
    transaction = Transaction()
    transaction.add_transaction(tr)
    return transaction
# Create your tests here.
#def test_skipyear():
#    year_split = line_from_csv(13)
#    tr = TransactionReader(year_split)
#    transaction = Transaction()
#    transaction.add_transaction(tr)
#    assert transaction.status == Transaction.STATUS_UNKNOWN_CHAOS_NR

class TransactionReaderTest(TestCase):
    def test_chaos_nr_regex(self): # these should match on regex(s) for chaos number/mitgliedsbeitrag w/ high confidence
        for i in range(3):
            tr = make_transaction_reader(i)
            self.assertLess(tr.rating, 8)

    def test_number_mashing(self):
        transaction = make_transaction(4)
        self.assertIn(transaction.chaos_number, [15, 16])
        self.assertEqual(transaction.status, transaction.STATUS_UNKNOWN_CHAOS_NR)

    def test_year_split_year5(self):  # wz: Mitgliedsbeitraege: 8001 (2014, 201 5)
        transaction = make_transaction(5)
        self.assertEqual(transaction.chaos_number, 8001)
        self.assertEqual(transaction.status, Transaction.STATUS_MATCHED_CHAOS_NR)

    def test_year_split_year6(self):  # mitgliedschaftsbeitrag 2014+2015 f. chaosnr. 5003
        transaction = make_transaction(6)
        self.assertEqual(transaction.chaos_number, 5003)
        self.assertEqual(transaction.status, Transaction.STATUS_MATCHED_CHAOS_NR)

    def test_year_split_year7(self):  # Mitgliedsbeitraege (2014, 201 5)
        transaction = make_transaction(7)
        self.assertIn(transaction.chaos_number, [201, 5])
        self.assertEqual(transaction.status, Transaction.STATUS_UNKNOWN_CHAOS_NR)

    def test_donation(self):
        transaction = make_transaction(11)
        self.assertEqual(transaction.status, Transaction.STATUS_IGNOREFOREVER)

    def test_year_after_chaos_key(self):
        transaction = make_transaction(12)
        self.assertEqual(transaction.status, Transaction.STATUS_UNKNOWN_CHAOS_NR) # erring with no false positives
        self.assertEqual(transaction.chaos_number, 2015)

    def test_skip_year(self):
        transaction = make_transaction(13) # corresponds to line number in csv, add lines only to end of csv!!
        self.assertEqual(transaction.status, Transaction.STATUS_UNKNOWN_CHAOS_NR)
        self.assertEqual(transaction.chaos_number, 2014)

    def test_multi_numbers(self):
        transaction = make_transaction(14) #Verwendungszweck 2014 3000 ..
        self.assertEqual(transaction.status, Transaction.STATUS_MATCHED_CHAOS_NR)
        self.assertEqual(transaction.chaos_number, 3000)

    def test_multi_numbers_chaosnr(self):
        transaction = make_transaction(15) #Verwendungszweck 2014 chaosnr 3000 ..
        self.assertEqual(transaction.status, Transaction.STATUS_MATCHED_CHAOS_NR)
        self.assertEqual(transaction.chaos_number, 3000)
