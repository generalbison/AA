from import_app.models import Transaction
from members.models import Member

def book_transaction(transaction, only_on_name_match=True):
    """
    attempts to book pending transactions to members account
    """
    if not isinstance(transaction, Transaction):
        raise ValueError

    mem_match = Member.objects.get(chaos_number=transaction.chaos_number)  #todo try/except
    transactor_names = transaction.payer.split()
    # proceed on matching name, or if name match not required
    proceed = False
    if not only_on_name_match:
        proceed = True
    elif mem_match.member_last_name in transactor_names:
        proceed = True

    if proceed:
        # todo, do I need anything in here that can actually check the balance change went through?
        mem_match.account_balance += transaction.amount
        transaction.status = transaction.STATUS_COMPLETED
    else:
        transaction.status = transaction.STATUS_ERRONEOUS
        # todo, I was thinking there'd be a booking admin/view interface where the user can apply this function to:
        # a) book everything below a particular rating (or marked as pending) without checking names (cuz # is better)
        # and b) book everything between two ratings with a name check
        # (because if you aren't sure you have the right number...)
        # and perhaps c) try other numbers from verwendungszweck that weren't successfully identified as chaos-nr
