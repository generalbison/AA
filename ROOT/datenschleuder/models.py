from django.db import models
from members.countryfield import CountryField

# Create your models here.
class Subscriber(models.Model):
    IMPORT_INITIAL = 'SYN'
    UNKNOWN_SOURCE = 'UKS'

    datenschleuder_number = models.AutoField(unique=True, primary_key=True)

    SUBSCRIBER_SOURCE_CHOICES = (
        (UNKNOWN_SOURCE, 'UNKNOWN SOURCE'),
        (IMPORT_INITIAL, 'INITIAL IMPORT'),
    )
    subscriber_source = models.CharField(max_length=3,
        choices=SUBSCRIBER_SOURCE_CHOICES
    )

    first_name = models.CharField(blank=False, null=False, max_length=255)
    last_name = models.CharField(blank=False, null=False, max_length=255)

    address_1 = models.CharField(blank=False, null=False, max_length=255)
    address_2 = models.CharField(blank=True, null=True, max_length=255)
    address_3 = models.CharField(blank=True, null=True, max_length=255)
    address_country = CountryField()

    address_unknown = models.BooleanField(default=False)
    address_unknown.short_desciption = 'will be set to FALSE on next address change'

    max_datenschleuder_issue = models.IntegerField(blank=False, null=False, default=0)  # inclusive last number
    max_datenschleuder_issue.short_desciption = 'number of the latest isssue this person will get (inclusive!)'
    
    is_endless = models.BooleanField(default=False)
    is_endless.short_description = 'free subscription'
    
    rt_link = models.URLField(max_length=60, null=True, blank=True, default='')
    
    def __str__(self):
        desc = str(self.datenschleuder_number) + ': ' + self.first_name + ' ' + self.last_name
        if self.is_endless:
            desc += ' (endless subscription)'
        else:
            desc += ' (' + str(self.max_datenschleuder_issue) + ')'
        return desc
    
    def will_receive_issue(self, issue):
        return self.is_endless or self.max_datenschleuder_issue >= issue
    
    # abo: x-ausgaben (def 8)
    # until issue number (example: 106)
    # 999 endless
    # bei export musss aktuelle datenschleuder nr angegeben werden!!!1!
