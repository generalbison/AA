from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^mail_send_next/', views.mail_send_next, name='mail_send_next'),
    url(r'^mail_send_all/', views.mail_send_all, name='mail_send_all'),
    url(r'^mail_create/', views.mail_create, name='mail_create'),
    url(r'^mail_still_to_send/', views.mail_still_to_send, name='mail_still_to_send'),
    url(r'^billing_cycle/', views.billing_cycle, name='billing_cycle'),
    url(r'^member_info/', views.member_info, name='member_info'),
    url(r'^zip_analysis/', views.zip_analysis, name='zip_analysis'),
    url(r'^country_analysis/', views.country_analysis, name='country_analysis'),
    url(r'^search_member_db/', views.search_member_db, name='search_member_db'),
    url(r'^get_erfa_statistics/', views.get_erfa_statistics, name='get_erfa_statistics'),
    url(r'^clear_members/', views.clear_members, name='clear_members'),
    url(r'^drop_transactions/', views.drop_transactions, name='drop_transactions'),
    url(r'^address_unknown/', views.bulk_address_unknown, name='api_bulk_address_unknown'),
    url(r'^send_data_record/', views.member_queue_data_record_email, name='send_data_record'),
    url(r'^member_inactivate/', views.member_inactivate, name='member_inactivate'),
    url(r'^member_reactivate/', views.member_reactivate, name='member_reactivate'),
]
