# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.db.models import Count
from django.core.mail import EmailMessage

from ROOT.settings import FEE, FEE_REDUCED, FEE_NOTIFICATION_TIME
from ROOT.settings import EMAIL_MANAGING_DIRECTOR, GPG_MANAGING_DIRECTOR, EMAIL_HOST_USER, GPG_HOST_USER, GPG_HOME
import ROOT.helper as helper
from ROOT.helper import i_request, str2bool
from members.models import EmailToMember
from members.models import Member
from members.models import BillingCycle
from members.models import EmailAddress
from members.models import Erfa
from members.models import send_or_refresh_data_record

from django.shortcuts import HttpResponse

from django.core import exceptions

# get_template is what we need for loading up the template for parsing.
from django.template.loader import get_template
from django.template.loader import select_template
from django.db.models import F, Q
# Templates in Django need a "Context" to parse with, so we'll borrow this.
# "Context"'s are really nothing more than a generic dict wrapped up in a
# neat little function call.
from django.template import Context
from django.http import JsonResponse

import time
import datetime
import json
import re
import gnupg
try:
    gpg = gnupg.GPG(gnupghome=GPG_HOME)
except TypeError as e:
    gpg = gnupg.GPG(homedir=GPG_HOME)
gpg.encoding = 'utf8'


RETURN_TYPE_JSON = 'json'
RETURN_TYPE_HTML = 'html'

MAIL_CREATE_NEW_USER = 'new_user'
MAIL_CREATE_DELAYED_PAYMENT = 'delayed_payment'


def _get_return_type(request):
    return_as = RETURN_TYPE_JSON
    if 'return_as' in i_request(request):
        return_as = i_request(request)['return_as']

    return return_as


def index(request):
    return render(request, 'api/index_api.html', {})


def country_analysis(request):
    return_as = _get_return_type(request)

    from members.countryfield import COUNTRIES
    country_dict = {}
    for member in Member.objects.members_only():
        if member.address_country not in country_dict:

            country_name = ''
            for countryPair in COUNTRIES:
                if countryPair[0] == member.address_country:
                    country_name = countryPair[1]
            country_dict[member.address_country] = {'country': member.address_country,
                                                    'country_name': country_name,
                                                    'members': 0}
        country_dict[member.address_country]['members'] += 1

    if return_as == RETURN_TYPE_JSON:
        json_list = []
        # for countryKey in sorted(country_dict):
        #     json_list.append(country_dict[countryKey])
        for countryKey in sorted(country_dict):
            json_list.append([countryKey, country_dict[countryKey]['country_name'],
                              country_dict[countryKey]['members']])

        return HttpResponse(json.dumps(json_list), content_type='application/json')

    elif return_as == RETURN_TYPE_HTML:
        pass

    return HttpResponse('BAD FORMAT', content_type='text/html')


def zip_analysis(request):
    return_as = _get_return_type(request)

    country = 'DE'
    if 'country' in i_request(request):
        country = i_request(request)['country']

    member_list = Member.objects.filter(address_country=country)

    zip_member_count_dict = {}
    zip_member_count_total = 0
    zip_count_total = 0
    for member in member_list:
        if member.is_member():
            plz = member.get_plz()
            if plz != '':
                plz = plz[:2]
                if plz not in zip_member_count_dict:
                    zip_member_count_dict[plz] = 0
                    zip_count_total += 1
                zip_member_count_dict[plz] += 1
                zip_member_count_total += 1

    zip_member_count_list = []
    for item in zip_member_count_dict.items():
        zip_member_count_list.append(item)

    if return_as == RETURN_TYPE_JSON:
        return HttpResponse(json.dumps({'zip_member_count_list': zip_member_count_list,
                                        'zip_member_count_total': zip_member_count_total,
                                        'zip_count_total': zip_count_total}), content_type='application/json')

    elif return_as == RETURN_TYPE_HTML:
        parsed_template = get_template('api_response_table_simple.html').render(
            Context({'dataList': zip_member_count_list})
        )

        return HttpResponse('<p>Total Members: ' + str(zip_member_count_total) + '<br />Total ZIP Codes:'
                            + str(zip_count_total) + '</p>' + parsed_template, content_type='text/html')

    return HttpResponse('BAD FORMAT', content_type='text/html')


def member_info(request):
    """
    get infos about a member by chaos_number

    chaos_number
    separator

    :param request:
    :return:
    """

    separator = '<br>'

    if 'separator' in i_request(request):  # request.REQUEST:
        separator = i_request(request)['separator']  # request.REQUEST['separator']
    if separator == '\\n':
        separator = '\n'

    all_infos = ''
    if 'chaos_number' in i_request(request):
        member = Member.objects.get(chaos_number=int(i_request(request)['chaos_number']))
        all_infos += str(member) + separator
        all_infos += 'Name: ' + member.get_name() + separator
        all_infos += 'Address: ' + member.get_address() + separator
        all_infos += 'Country: ' + member.address_country + separator
        all_infos += 'PLZ: ' + member.get_plz() + separator
        if member.email_is_unknown():
            all_infos += 'email is unknown' + separator
        else:
            all_infos += separator.join(str(v) for v in member.get_emails())
            # seperator.join(member.getEmails()) + seperator

    if all_infos == '':
        all_infos = 'nothing to do ' + ', '.join(i_request(request))

    return HttpResponse(all_infos, content_type='text')


def member_queue_data_record_email(request):
    """
    Send a data record email to the specified Member(s) no matter if anything in their data set changed. Will refresh
     already queued emails instead of queueing multiple emails.
    :param request: Should be a GET request with one or more parameters 'chaos_number', being the chaos_number of a
    Member
    :return: A JSON string with a dictionary of each chaos number paired with either 'queued' or 'not found'
    """
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number)
            send_or_refresh_data_record(member)
            response[chaos_number] = 'queued'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found'
    return HttpResponse(str(response), content_type='text')


def member_inactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=True)
            member.is_active = False
            member.save()
            response[chaos_number] = 'inactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already inactive'
    return HttpResponse(str(response), content_type='text')


def member_reactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=False)
            member.is_active = True
            member.save()
            response[chaos_number] = 'reactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already active'
    return HttpResponse(str(response), content_type='text')


def billing_cycle(request):
    """
    :param request:
    :return: HttpResponse (json)
    """
    del request

    this_month = int(time.strftime('%Y%m'))

    paid_this_month = BillingCycle.objects.filter(last_run_int=this_month)
    member_list = Member.objects.all().exclude(membership_type=Member.MEMBERSHIP_TYPE_HONORARY)

    booked = []
    for member in member_list:
        # ToDo: members which left between the last billing cycle and now?
        if member.is_active and not paid_this_month.filter(member=member).exists():
            print('FOUND MEMBER:', member)

            if member.fee_paid_until < datetime.date.today():
                fee_value = member.get_annual_fee()
                print('fee_value:', fee_value)
            else:
                fee_value = 0
                print('fee_value: not due')

            next_billing_cycle = BillingCycle()
            next_billing_cycle.member = member
            next_billing_cycle.last_run = datetime.datetime.now()
            next_billing_cycle.last_run_int = this_month
            next_billing_cycle.fee_value = fee_value

            save_state = ''
            try:
                next_billing_cycle.save()
                booked.append({'member': str(member),
                               'memberId': member.chaos_number,
                               'memberName': member.get_name()})
            except Exception as ex:
                save_state = ex

            if save_state == '' and fee_value > 0:
                from members.models import BalanceTransactionLog
                from dateutil.relativedelta import relativedelta
                member.fee_paid_until += relativedelta(years=1)
                member.increase_balance_by(-fee_value, BalanceTransactionLog.BILLING_CYCLE, str(this_month))
            elif save_state != '':
                print('CRITICAL ERROR: CANT SAVE billing_cycle FOR:', member, 'ERROR:', save_state)

    return HttpResponse(json.dumps(booked), content_type='application/json')


def _mail_create_by_id(mail_type, u_id):
    return _mail_create(mail_type, u_id)


def mail_create(request):
    mail_type = ''
    user_id = -1
    if MAIL_CREATE_NEW_USER in i_request(request):
        mail_type = MAIL_CREATE_NEW_USER
        user_id = i_request(request)[MAIL_CREATE_NEW_USER]
    elif MAIL_CREATE_DELAYED_PAYMENT in i_request(request):
        mail_type = MAIL_CREATE_DELAYED_PAYMENT

    response = _mail_create(mail_type, user_id)

    return HttpResponse(json.dumps(response), content_type='application/json')


def _get_delayed_payment_members():
    min_in_debt = helper.get_fee_monthly(FEE) * FEE_NOTIFICATION_TIME
    # return Member.objects.filter(account_balance__lte=-min_in_debt, fee_override=0).exclude(
    #     membership_type=Member.MEMBERSHIP_TYPE_HONORARY) | Member.objects.filter(
    #     account_balance__lte=(int(F('fee_override')) / 12) * FEE_NOTIFICATION_TIME).exclude(
    #     membership_type=Member.MEMBERSHIP_TYPE_HONORARY)

    delayed_members = Member.objects.filter(
        account_balance__lte=-min_in_debt, fee_override__isnull=True).exclude(
        membership_type=Member.MEMBERSHIP_TYPE_HONORARY) | Member.objects.filter(
        fee_override__isnull=False, account_balance__lte=(F('fee_override') / 12) * FEE_NOTIFICATION_TIME).exclude(
        membership_type=Member.MEMBERSHIP_TYPE_HONORARY).exclude(is_active=False)

    delayed_members = delayed_members.exclude(erfa__has_doppelmitgliedschaft=True)
    return delayed_members


def _mail_create(mail_type, user_id):
    response = []
    member_list = []
    context_dict = {}
    # subject = ''
    # template_prefix = ''
    # template_suffix = '.txt.html'
    # mail_to_send_type = EmailToMember.SEND_TYPE_DEFAULT

    mails_skipped = 0
    mails_errors = 0
    mails_added = 0

    if MAIL_CREATE_DELAYED_PAYMENT == mail_type:
        # member_list = Member.objects.filter(
        #     account_balance__lte=-(FEE_NOTIFICATION_TIME*FEE), fee_is_reduced=False, fee_is_honorary_member=False,
        #     member_is_member=True) | Member.objects.filter(account_balance__lte=-(FEE_NOTIFICATION_TIME*FEE_REDUCED),
        #                                                    fee_is_reduced=True, fee_is_honorary_member=False,
        #                                                    member_is_member=True)
        #     .filter(account_balance__lte=-(2*FEE_REDUCED), fee_is_reduced=True, fee_is_honorary_member=False)
        # print(Member.objects\
        #    .filter(account_balance__lte=-(2*FEE), fee_is_reduced=False, fee_is_honorary_member=False)\
        #    .filter(account_balance__lte=-(2*FEE_REDUCED), fee_is_reduced=True, fee_is_honorary_member=False).query)
        # FEE_NOTIFICATION_TIME
        # entered__gte=datetime.now()-timedelta(days=how_many_days)
        #
        # # old payment model
        # member_list = Member.objects.filter(
        #     membership_supporter=False, fee_paid_until__lte=datetime.datetime.now()-datetime.timedelta(
        #         days=FEE_NOTIFICATION_TIME*30)) | Member.objects.filter(
        #     fee_paid_until__isnull=True, membership_start__lte=datetime.datetime.now()-datetime.timedelta(
        #         days=FEE_NOTIFICATION_TIME*30))

        member_list = _get_delayed_payment_members()
        # member_list = member_list.exclude(fee_override = 0)
        # Member.objects.filter(membership_supporter=False, account_balance__lte=helper.getFeeMonthly(
        #     F('fee_override')) * FEE_NOTIFICATION_TIME)
        # print(member_list)

        subject = 'Zahlungsverzug'  # TODO: Translate!
        template_prefix = 'mail_templates/mail_delayedpayment_'
        mail_to_send_type = EmailToMember.SEND_TYPE_DELAYED_PAYMENT
        # +2 month AND not empty
    else:
        # return HttpResponse(json.dumps({'state': 'error'}), content_type='application/json')
        return {'state': 'error'}

    members_i_cannot_mail = []
    for member in member_list:
        if not member.is_active:  # yeah, looks bad, waiting for an better idea to do that -.-
            continue
        context_dict['member'] = member

        # fee_per_day = 0
        # if member.membership_reduced:
        #     fee_per_day = FEE_REDUCED / 366
        # else:
        #     fee_per_day = FEE / 366

        # TODO: complete the calculation!
        context_dict['toPay'] = '{0:.2f}'.format(member.get_money_to_pay() / 100)

        template = select_template([template_prefix + member.address_country + '.txt.html',
                                    template_prefix + 'EN.txt.html'])
        parsed_template = template.render(
            Context(context_dict)
        )

        try:
            mail = member.get_primary_mail()
            if not mail:
                # member has no mail address, i don't care!
                mails_skipped += 1
                members_i_cannot_mail.append('has no Email Address: ' + str(member))
                continue
            msg = EmailToMember()

            msg.member = member
            msg.body = parsed_template
            msg.subject = subject
            msg.created = datetime.datetime.now()
            msg.email_type = mail_to_send_type

            try:
                # no problem with multiple existing mailadresses, because foreign key=member/chaosnr
                msg_exists = EmailToMember.objects.filter(member=member, subject=subject)
                if msg_exists.count() > 0:
                    mails_skipped += 1
                    members_i_cannot_mail.append('SKIPPED: Mail with subject "' + subject +
                                                 '" already exists in queue for: ' + str(member))
                else:
                    mails_added += 1
                    msg.save()
                    response.append(str(mail.email_address))

            except Exception as ex:
                mails_errors += 1
                print(ex)
                members_i_cannot_mail.append('ERROR: checkExistingMails ' + str(member) + ' Ex: ' + str(ex))
        except exceptions.ObjectDoesNotExist:
            # member has no mail address, i don't care!
            members_i_cannot_mail.append('has no Email Address: ' + str(member))
            mails_skipped += 1
        except Exception as ex:
            mails_errors += 1
            print(ex)
            members_i_cannot_mail.append('ERROR: ' + str(member) + ' Ex: ' + str(ex))

    return ['mails_added: ' + str(mails_added), 'mails_skipped: ' + str(mails_skipped),  'mails_errors: ' +
            str(mails_errors)] + members_i_cannot_mail + response

    # return render_to_response(
    #     'api_response.html',
    #     { 'title': 'mail_create', 'msg': response },
    #     context_instance=RequestContext(request)
    # )


def mail_send_next(request):
    del request

    mail_to_send = EmailToMember.objects.first()
    if mail_to_send:
        response = mail_to_send.send()
    else:
        response = {'state': 'nothing to send'}
    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_send_all(request):
    del request

    response = [mail.send() for mail in EmailToMember.objects.all()]

    if len(response) == 0:
        response = {'state': 'nothing to send'}

    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_still_to_send(request):
    del request

    number_of_mails = EmailToMember.objects.all().count()

    return HttpResponse(json.dumps({'numberOfMailsToSend': number_of_mails}), content_type='application/json')


def search_member_db(request):
    """Handles member DB search requests returning HTML-table-rows"""
    context = {}
    if request.method == 'POST':
        params = request.POST
        check_empty_first_name = params.get('check_empty_first_name')
        check_empty_last_name = params.get('check_empty_last_name')
        check_empty_address = params.get('check_empty_address')
        check_empty_country = params.get('check_empty_country')
        check_empty_email = params.get('check_empty_email_address')
        if 'true' in (check_empty_address, check_empty_country, check_empty_first_name, check_empty_last_name,
                      check_empty_email):
            # Search for empty fields
            q = Member.objects.members_only()
            if check_empty_first_name == 'true':
                q = q.filter(Q(first_name__exact='') | Q(first_name__isnull=True))
                print(q)
            if check_empty_last_name == 'true':
                q = q.filter(Q(last_name__exact='') | Q(last_name__isnull=True))
            if check_empty_address == 'true':
                # Filter for datasets with all three address field empty
                q = q.filter(
                    (
                        (Q(address_1__exact='') |
                         Q(address_1__isnull=True)) &
                        (Q(address_2__exact='') |
                         Q(address_2__isnull=True)) &
                        (Q(address_3__exact='') |
                         Q(address_3__isnull=True))
                    ) | Q(address_unknown=True)
                )
            if check_empty_country == 'true':
                q = q.filter(Q(address_country__exact='') | Q(address_country__isnull=True))
            if check_empty_email == 'true':
                members = EmailAddress.objects.all().values('member')
                m = Member.objects.all()
                for member in members:
                    m = m.exclude(chaos_number__exact=member['member'])
                q = q & m

            context['results'] = q
        else:
            first_name = params.get('first_name', '')
            last_name = params.get('last_name', '')
            chaos_number = params.get('chaos_id', '')
            address = params.get('address', '')
            email = params.get('email_address', '')
            fee_reduced = str2bool(params.get('fee_is_reduced', 'false'))
            is_active = str2bool(params.get('is_active', 'false'))
            apply_filters = str2bool(params.get('apply_filters', 'false'))
            q = Member.objects.members_only()
            if first_name != '':
                q = q.filter(first_name__icontains=first_name)
            if last_name != '':
                q = q.filter(last_name__icontains=last_name)
            if chaos_number != '':  # maybe check for illegal characters in the future
                q = q.filter(chaos_number=chaos_number)
            if address != '':
                q = q.filter(Q(address_1__icontains=address) | Q(address_2__icontains=address)
                             | Q(address_3__icontains=address))
            if email != '':
                q = q.filter(Q(email__email_address__icontains=email))
            if apply_filters:
                q = q.filter(membership_reduced=fee_reduced, is_active=is_active)

            context['results'] = q
        return render(request, 'api/member_search_result.html', context)
    return HttpResponse('Neither Get nor Post')  # HttpResponse('BAD FORMAT', content_type='text/html')


def _erfa_statistics():
    erfa_statistics = {}
    members = Member.objects.members_only()  # .exclude(Q(membership_end__lt=datetime.datetime.now()))
    counts = members.all().annotate(number_members=Count('erfa'))  # .order_by('total')
    counts = counts.values('membership_type', 'erfa').annotate(num_type_members=Count('erfa'))
    all_erfas = Erfa.objects.all()

    for e in all_erfas.values('id'):
        # Erfa-Kreise sind Erfas ohne Doppelmitgliedschaft und Vereine sind Erfa-Kreise mit Doppelmitgliedschaft.
        erfa_prefix = 'Erfa-Kreis'
        erfa = Erfa.objects.get(pk=e['id'])
        if erfa.has_doppelmitgliedschaft:
            erfa_prefix = 'Verein'
        erfa_statistics[erfa.get_short_long_name()] = {
            'erfa_prefix': erfa_prefix,
            'monthly_income_total': 0,
            'monthly_income_full': 0,
            'monthly_income_reduced': 0,
            'monthly_income_special': 0,
            Member.MEMBERSHIP_TYPE_SUPPORTER: 0,
            Member.MEMBERSHIP_TYPE_MEMBER: 0,
            Member.MEMBERSHIP_TYPE_HONORARY: 0,
            'payer_count_full': 0,
            'payer_count_reduced': 0,
            'payer_count_special': 0
        }

    # dead_erfa_name = 'DEAD ' + EMPTY_ERFA_NAME
    for c in counts:
        erfa_id = c['erfa']
        # erfa_name = ''

        # TODO: exited members are what? #89
        if erfa_id is None:
            pass
            # erfa_id = 1
            # erfa_name = 'Alien (Alien)'  # dead_erfa_name
        else:
            erfa_name = Erfa.objects.get(pk=erfa_id).get_short_long_name()  # slow due to nesting, but n<1000
            member_type = c['membership_type']
            number = c['num_type_members']
            erfa_statistics[erfa_name].update({member_type: number})
            members_of_erfa = members.filter(erfa=erfa_id)
            print(erfa_name)
            for erfa_member in members_of_erfa:

                member_pays = helper.get_fee_monthly(erfa_member.get_annual_fee()) / 100.0
                print(erfa_member, erfa_member.get_annual_fee(), member_pays)
                erfa_statistics[erfa_name]['monthly_income_total'] += member_pays
                if erfa_member.membership_reduced:
                    erfa_statistics[erfa_name]['payer_count_reduced'] += 1
                    erfa_statistics[erfa_name]['monthly_income_reduced'] += member_pays
                elif erfa_member.fee_override is not None:
                    erfa_statistics[erfa_name]['payer_count_special'] += 1
                    erfa_statistics[erfa_name]['monthly_income_special'] += member_pays
                else:
                    erfa_statistics[erfa_name]['payer_count_full'] += 1
                    erfa_statistics[erfa_name]['monthly_income_full'] += member_pays

    return erfa_statistics


def get_erfa_statistics(request):

    # yes, dirty but would do it for now'
    def get_all_annual_fees_from_list(member_list):
        total = 0
        for member in member_list:
            total += member.get_annual_fee()

        return total

    statistic = _erfa_statistics()
    print(statistic)
    if EMAIL_MANAGING_DIRECTOR != '':

        # context dict for template parsing
        # we need an EMPTY string to rjust/ljust it in the template
        context_dict = {'emptystring': '', 'statistic': statistic,
                        'statistics_created_for': datetime.date.today().strftime('%B %Y'),
                        'longest_erfa_text': max(len(k) for k, v in statistic.items() if len(v) != 0)}

        # get the overview
        paying_members = Member.objects.members_only().exclude(membership_type=Member.MEMBERSHIP_TYPE_HONORARY)
        members_full = paying_members.filter(
            membership_reduced=False,
            fee_override__isnull=True
        )
        members_reduced = paying_members.filter(
            membership_reduced=True,
            fee_override__isnull=True
        )
        members_fee_override = paying_members.filter(
            membership_reduced=False,
            fee_override__isnull=False
        )

        context_dict['count_members_full'] = members_full.count()
        context_dict['count_members_reduced'] = members_reduced.count()
        context_dict['count_members_fee_override'] = members_fee_override.count()
        context_dict['count_active_members_total'] = paying_members.count()

        income_members_full_cent = get_all_annual_fees_from_list(members_full)
        income_members_reduced_cent = get_all_annual_fees_from_list(members_reduced)
        income_members_fee_override_cent = get_all_annual_fees_from_list(members_fee_override)
        context_dict['income_members_full'] = '{:0.2f}'.format(float(income_members_full_cent) / 100.0)
        context_dict['income_members_reduced'] = '{:0.2f}'.format(float(income_members_reduced_cent) / 100.0)
        context_dict['income_members_fee_override'] =\
            '{:0.2f}'.format(float(income_members_fee_override_cent) / 100.0)
        income_yearly_cent = income_members_full_cent + income_members_reduced_cent\
            + income_members_fee_override_cent
        context_dict['income_yearly'] = '{:0.2f}'.format(float(income_yearly_cent) / 100.0)
        context_dict['income_monthly'] = '{:0.2f}'.format(float(income_yearly_cent / 12) / 100.0)

        delayed_payment_members = _get_delayed_payment_members()

        context_dict['delayed_payment_notification_time'] = FEE_NOTIFICATION_TIME
        context_dict['delayed_payment_members'] = delayed_payment_members.filter(
            membership_reduced=False,
            fee_override__isnull=True
        ).count()
        context_dict['delayed_payment_members_reduced'] = delayed_payment_members.filter(
            membership_reduced=True,
            fee_override__isnull=True
        ).count()
        context_dict['delayed_payment_members_fee_override'] = delayed_payment_members.filter(
            membership_reduced=False,
            fee_override__isnull=False
        ).count()

#             - Mitgliedern .........: {{ income_members_full|rjust:7 }} EUR
#             - erm. Mitgliedern.....: {{ income_members_reduced|rjust:7 }} EUR
#             - erm. Mitglieder Spez.: {{ income_members_fee_override|rjust:7 }} EUR
#                          ===========
#                   Total: {{ income_yearly }} EUR  Dies entspricht {{ income_monthly }} EUR im Monat.
#
#                   - Mitglieder ..........: {{ delayed_payment_members|rjust:5 }}
# - erm. Mitglieder......: {{ delayed_payment_members_reduced|rjust:5 }}
# - erm. Mitglieder Spez.: {{ delayed_payment_members_fee_override|rjust:5 }}
#

        # managing_member = Member.objects.get(pk=chaos_number)
        # context_dict['member'] = managing_member
        # managing_member = Member()
        # new_ets = EmailToMember(member=managing_member, email_type=EmailToMember.DEFAULT)
        # new_ets.subject = 'Erfa-Statistik für {}'.format(context_dict['statistics_created_for'])

        template = select_template(['mail_templates/mail_erfa_statistic.html', ])
        parsed_template = template.render(
            Context(context_dict)
        )

        gpg.recv_keys('hkp://pgp.mit.edu', GPG_MANAGING_DIRECTOR)
        gpg.recv_keys('hkp://pgp.mit.edu', GPG_HOST_USER)
        encrypted_body = gpg.encrypt(parsed_template, [GPG_MANAGING_DIRECTOR, GPG_HOST_USER],
                                     always_trust=True).data.decode('ascii')

        # new_ets.body = parsed_template
        send_email = EmailMessage(
            subject='Monatliche Statistik',
            body=encrypted_body,
            from_email=EMAIL_HOST_USER,
            to=[EMAIL_MANAGING_DIRECTOR, ],
            bcc=[EMAIL_HOST_USER, ]
        )
        send_email.send(False)

        return HttpResponse('<pre>' + parsed_template + '</pre>')
        # new_ets.save()

    return JsonResponse(statistic)


def create_delayed_payment_export():
    member_list = _get_delayed_payment_members()
    # final_file_string = ''
    import csv

    with open('/tmp/delayed_payment_export.csv', 'w', newline='\n') as csvfile:
        export_writer = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        export_writer.writerow(['chaos_number',
                                'first_name',
                                'last_name',
                                'address_1',
                                'address_2',
                                'address_3',
                                'address_country',
                                'state',
                                'entry_date',
                                'yearly_fee',
                                'last_payment',
                                'paid_until',
                                'balance',
                                'open_years',
                                'fee_to_pay'])
        for member in member_list:
            row_arr = [member.chaos_number, member.first_name, member.last_name, member.address_1, member.address_2,
                       member.address_3, member.address_country, 'aktiv', member.membership_start,
                       member.get_annual_fee(), member.fee_last_paid, member.fee_paid_until,
                       member.get_balance_readable(), 7, member.get_money_to_pay()]
            export_writer.writerow(row_arr)


def clear_members(request):
    _request = i_request(request)

    response = {}
    if 'deleteMember' in _request:
        response = _member_exit(_request['deleteMember'])
    elif 'deleteMembers' in _request:
        for memberId in _request['deleteMembers']:
            response['memberId'] = _member_exit(memberId)

    return JsonResponse(response)


def _member_exit(chaos_number):
    error_message = ''
    try:
        m = Member.objects.get(chaos_number__exact=chaos_number)
        m.exit()
        m.save()
        was_ok = True
    except Exception as ex:
        was_ok = False
        error_message = str(ex)

    return {'error_message': error_message, 'was_ok': was_ok}


def drop_transactions(request):
    # http://localhost:8000/api/drop_transactions?reallydroptransactions=yes
    _request = i_request(request)
    response = {'errorMessage': '', 'wasCleared': False, 'transactionsDropped': 0}
    if 'reallydroptransactions' in _request and _request['reallydroptransactions'] == 'yes':
        try:
            from import_app.models import Transaction
            all_transactions = Transaction.objects.all()
            count_transactions = all_transactions.count()
            all_transactions.delete()
            response['transactionsDropped'] = count_transactions
            response['wasCleared'] = True
        except Exception as ex:
            response['errorMessage'] = str(ex)

    return JsonResponse(response)


def bulk_address_unknown(request):
    changes = {'success': 0, 'error': 0, 'no_data': False}
    if request.method == 'POST':
        chaos_numbers = request.POST['chaos_numbers'].split('\n')
        for chaos_number in chaos_numbers:
            nr = re.search('([0-9]+)', chaos_number).group(1)
            print(nr)
            m = Member.objects.get(chaos_number__exact=nr)
            print(m)
            try:
                m.address_unknown = True
                m.save()
                changes['success'] += 1
            except:
                changes['error'] += 1
    else:
        changes['no_data'] = True
    return JsonResponse(changes)
