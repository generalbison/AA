from django.forms import ModelForm
from .models import Member
from .models import BalanceTransactionLog

class BalanceTransactionLogForm(ModelForm):
    class Meta:
        model = BalanceTransactionLog
        fields = ['member', 'changed_value', 'new_value', 'comment']

    def __init__(self, *args, **kwargs):
        super(BalanceTransactionLogForm, self).__init__(*args, **kwargs)
