from .models import Member, Erfa, EmailAddress
from csv import writer, reader
import logging
import math
import pendulum
import datetime


def uglify_date(date):
    return "{:%d.%m.%y}".format(date)


class MemberCSVHelper:

    fieldnames = ()

    def __init__(self, filename, member_set=None):
        self.filename = filename
        self.member_set = member_set
        self.writer = None
        self.reader = None
        self._fh = None
        self.warnings = []
        self.logs = []

    def do_export(self):
        assert self.member_set is not None, "We need members if we want to export them"
        self.setup_writer()
        for member in self.member_set:
            self.writer.writerow(self.get_row(member))
        self._fh.close()

    def do_import(self):
        self.setup_reader()
        for row in self.reader:
            self.import_row(row)

    def _get_writer(self,):
        return writer(self._fh)

    def setup_writer(self,):
        if not self._fh:
            self._fh = open(self.filename, 'w')
            self.writer = self._get_writer()
            self.writer.writerow(self.fieldnames)

    def _get_reader(self,):
        return reader(self._fh)

    def setup_reader(self,):
        if not self._fh:
            if hasattr(self.filename, 'read'):
                self._fh = self.filename
            else:
                self._fh = open(self.filename)
            self.reader = self._get_reader()
            first_row = next(self.reader)
            if not tuple(self.fieldnames) == tuple(first_row):
                self.reader = None
                self._fh.close()
                self._fh = None
                assert False, "fieldnames mismatch\nexpected: {}\ngot: {}".format(self.fieldnames, first_row)

    def get_row(self, member):
        raise NotImplementedError

    def import_row(self, row):
        member = self.get_member_from(row)
        if self.update_member(member, row):
            member.save()
            print("updated, member {}".format(member))
            self.logs.append("updated, member {}".format(member))
        print(self.logs)

    def get_member_from(self, row):
        raise NotImplementedError

    def update_member(self, member, row):
        raise NotImplementedError


class ErfaAbgleich(MemberCSVHelper):
    fieldnames = ("Änderung", "Eintrittsdatum CCC", "CNR", "Status", "Vorname",
                  "Nachname", "Anschrift1", "Anschrift2", "Anschrift3", "E-Mail")

    def __init__(self, filename, erfa, member_set=None):
        if member_set is None:
            member_set = erfa.member_set
        self.erfa = erfa
        super().__init__(filename, member_set)

    def get_row(self, member):
        change = ""
        membership_start = member.membership_start
        chaos_number = member.chaos_number
        first_name = member.first_name
        last_name = member.last_name
        address_1 = member.address_1
        address_2 = member.address_2
        address_3 = member.address_3
        country = member.address_country
        email = member.get_primary_mail()
        if not member.is_active:
            status = "Ruhend"
        elif member.membership_type == Member.MEMBERSHIP_TYPE_HONORARY:
            status = "Ehrenmitglied"
        elif member.membership_reduced:
            status = "Ermäßigt"
        else:
            status = "Vollmitglied"
        return (change, membership_start, chaos_number, status, first_name, last_name, address_1, address_2, address_3,
                country, email)

    def get_member_from(self, row):
        if not row[2]:
            return Member()
        return Member.objects.get(pk=int(row[2]))

    def update_member(self, member, row):
        change, membership_start, chaos_number, status, first_name, last_name, address_1, address_2, address_3, country, email = row
        if not change:
            # nothing changed, we do not need to save the model
            return False
        member.first_name = first_name
        member.last_name = last_name
        member.address_1 = address_1
        member.address_2 = address_2
        member.address_3 = address_3
        member.address_country = country
        EmailAddress(member=member, email_address=email, is_primary=True).save()
        if status == "Vollmitglied":
            member.membership_reduced = False
            member.fee_override = None
        elif status == "Ehrenmitglied":
            member.membership_type = Member.MEMBERSHIP_TYPE_HONORARY
            member.membership_reduced = False
            member.fee_override = None
        elif status == "Ermäßigt":
            member.fee_override = None
            member.membership_reduced = True
        elif status == "Ruhend":
            member.is_active = False
            if member.account_balance < 0:
                # member will be saved after this
                member.set_balance_to(0)
                member.fee_paid_until = datetime.date.today()
        elif status == "Austritt Erfa":
            member.alienate(self.erfa)
        elif status == "Austritt CCC":
            member.exit()
        else:
            warn = "Member status unknown, got '{status}': {first} {last} ({pk})".format(
                status=status, first=first_name, last=last_name, pk=chaos_number)
            logging.warning(warn)
            self.warnings.append(warn)

        member.last_update = pendulum.date.today()

        return True


class CashPoint(MemberCSVHelper):

    fieldnames = ('ORG', 'VORNAME', 'NACHNAME', 'ADRESSE_1', 'ADRESSE_2', 'ADRESSE_3',
                  'CHAOSNR', 'EMAIL', 'ERFA', 'ART', 'EINTRITT', 'LETZTEZAHLUNG', 'BEZBIS',
                  'LASTUPDATE', 'JAHREOFFEN', 'JAHRTEXT', 'SUMME', 'DANNBIS', 'PGPKEYID', 'FOERDER')

    # export

    def _get_writer(self,):
        return writer(self._fh, delimiter="\t")

    def get_row(self, member):
        email = ''
        gpg_key_id = ''
        primary_mail = member.get_primary_mail()
        if primary_mail is not None:
            email = primary_mail.email_address
            gpg_key_id = primary_mail.gpg_key_id

        organization = ""
        first_name = member.first_name
        last_name = member.last_name
        address_1 = member.address_1
        address_2 = member.address_2
        address_3 = member.address_3
        chaos_number = member.chaos_number
        # email
        erfa = member.erfa.short_name
        if not member.is_active:
            membership_type = "ruhe"
        elif member.membership_type == Member.MEMBERSHIP_TYPE_HONORARY:
            membership_type = "ehre"
        elif member.membership_reduced:
            membership_type = "erm"
        else:
            membership_type = "voll"
        membership_begin = member.membership_start
        membership_end = member.fee_last_paid
        paid_until = member.fee_paid_until
        account_balance = member.get_balance_readable()
        last_update = uglify_date(member.last_update)
        if member.fee_paid_until is None:
            payment_due_date = pendulum.date.today() - member.membership_start
        else:
            payment_due_date = pendulum.date.today() - member.fee_paid_until
        annual_fees_due = max(math.ceil(payment_due_date.days / 365), 0)
        if annual_fees_due == 0:
            years_label = ''
        elif annual_fees_due == 1:
            years_label = 'Jahresbetrag'
        else:
            years_label = 'Jahresbetraege'
        amount_due = member.get_money_to_pay_readable()
        next_payment_due_date = uglify_date(
            pendulum.instance(paid_until).add(years=annual_fees_due))
        is_supporter = 'yes' if member.membership_type == member.MEMBERSHIP_TYPE_SUPPORTER else 'no'

        return (
            organization, first_name, last_name, address_1, address_2, address_3, chaos_number, email, erfa,
            membership_type, membership_begin, membership_end, paid_until, account_balance, last_update,
            annual_fees_due, years_label, amount_due, next_payment_due_date, gpg_key_id, is_supporter
        )

    def get_member_from(self, row):
        # if there is no chaos number, we create a new Member
        if not row[6]:
            return Member()
        return Member.objects.get(pk=int(row[6]))

    def update_member(self, member, row):
        # there is no import for this :P
        return False


class Vereinstisch(MemberCSVHelper):

    def get_row(self, member):
        pass

    def get_member_from(self, row):
        if not row[2] or int(row[2]) <= 0:
            return Member()
        return Member.objects.get(pk=int(row[2]))

    def update_member(self, member, row):
        (client_id, server_id, chaosnummer, sprache, betrag, jahre, foerdermitglied, ermaessigt, neumitglied,
         nachzahler, datenaenderung, vorname, nachname, adresse_1, adresse_2, adresse_3, email, erfa, timestamp,
         gpg, einzahlung, bezahlt_bis) = row

        # TODO: land, plz, stadt, strasse => adresse_1, adresse_2, adresse_3

        member.first_name = vorname
        member.last_name = nachname
        member.address_1 = adresse_1
        member.address_2 = adresse_2
        member.address_3 = adresse_3

        # TODO: should we delete emails if this field is empty?
        # should we delete all, or just the primary?
        EmailAddress(member=member, email_address=email, gpg_key_id=gpg, is_primary=True).save()

        erfa = Erfa.objects.get(short_name=erfa)
        member.erfa = erfa

        if foerdermitglied:
            member.membership_type = Member.MEMBERSHIP_TYPE_SUPPORTER
        elif ermaessigt:
            member.fee_override = None
            member.membership_reduced = True
        else:
            member.membership_reduced = False
            member.fee_override = None

        member.last_update = pendulum.date.today()

        # TODO add transaction log
        member.account_balance += betrag * 100

        return True
