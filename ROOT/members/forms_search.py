from django import forms


class SearchForm(forms.Form):
    # Default search is AND
    # Optional OR
    # queryset.filter(field_name__icontains=bla)

    # Search Fields
    member_first_name = forms.CharField()
    member_last_name = forms.CharField()
    member_chaos_id = forms.CharField()
    member_address = forms.CharField()
    member_email_address = forms.CharField()

    # Filter Fields
    member_fee_reduced = forms.BooleanField()
    is_active = forms.BooleanField()
    apply_filters = forms.BooleanField()

    check_empty = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                            choices=(("first", "First name"),
                                                     ("last", "Last name"),
                                                     ("address", "Address"),
                                                     ("country", "Country"),
                                                     ("email", "Email address")))
    clear_results_before_query = forms.BooleanField(initial=True)

    pass
