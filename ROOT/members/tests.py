from django.test import TestCase
from members.models import Member, EmailAddress, EmailToMember, BillingCycle, Erfa, BalanceTransactionLog
import api.views as api_views
from ROOT.settings import FEE
from datetime import datetime, timedelta
# Create your tests here.


def mock_members(number_of_kinds, chaos_numbers):
    """Creates mock Member database with 4*numberOfKinds+1 members
    Args:
        number_of_kinds:
        chaos_numbers:

    Returns:

    """

    # members with positive account balance
    for i in range(number_of_kinds):
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir' + str(chaos_number),
            last_name='Las' + str(chaos_number),
            account_balance=chaos_number * 1000,
            membership_type = Member.MEMBERSHIP_TYPE_MEMBER
        )

    # members with negative account balance
    for i in range(number_of_kinds):
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Min' + str(chaos_number),
            last_name='Las Min' + str(chaos_number),
            account_balance=-(chaos_number * 1000),
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

    for i in range(number_of_kinds):
        # members with fee override and positive account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Cust' + str(chaos_number),
            last_name='Las Cust' + str(chaos_number),
            account_balance=chaos_number * 1000,
            fee_override=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

        # members with fee override and negative account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Fir Cust Min' + str(chaos_number),
            last_name='Las Cust Min' + str(chaos_number),
            account_balance=-(chaos_number * 1000),
            fee_override=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_SUPPORTER
        )

    # members that have been exited
    for i in range(number_of_kinds):
        # with positive account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Delete' + str(chaos_number),
            last_name='ME' + str(chaos_number),
            account_balance=chaos_number * 1000,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )

        api_views._member_exit(chaos_number=chaos_number)
        Member.objects.get(pk=chaos_number)
        # print(chaos_number, 'X *'*10, del_member, del_member.membership_end, del_member.first_name)
        # with negative account balance
        chaos_number = chaos_numbers.pop(0)
        Member.objects.create(
            chaos_number=chaos_number,
            first_name='Delete' + str(chaos_number),
            last_name='ME' + str(chaos_number),
            account_balance=-(chaos_number * 1000),
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER
        )
        api_views._member_exit(chaos_number=chaos_number)

    # MEMBERSHIP_TYPE_HONORARY don't bother with money!
    chaos_number = chaos_numbers.pop(0)
    Member.objects.create(
        chaos_number=chaos_number,
        first_name='HONORARY Cust Min' + str(FEE * 5),
        last_name='HONORARY Cust Min' + str(FEE * 5),
        account_balance=-(FEE * 5),
        membership_type=Member.MEMBERSHIP_TYPE_HONORARY
    )


class MemberTestCase(TestCase):

    def setUp(self):
        # how many members of each kind are created? (easier debugging)
        number_of_kinds = 2
        chaos_numbers = list(range(1000))  # pre-generate the chaos_numbers
        mock_members(number_of_kinds, chaos_numbers)
        Member.objects.create(
            chaos_number=133742,
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER,
            erfa=Erfa.objects.create(short_name="abc", long_name="argbleurkccippp"),
            first_name="anton",
            last_name="berger",
            address_1="Under the Bridge 23",
            address_country="DE",
            membership_start=datetime.utcfromtimestamp(1402531200),
            account_balance=4200,
            fee_override=250,
            comment="Dieser Nutzer hat doofe Ohren!"
        )

        for member in Member.objects.all():
            if member.first_name != '':
                email = (member.first_name + member.last_name).replace(' ', '_') + '@' + 'example.com'
                EmailAddress(member=member, email_address=email).save()

    @staticmethod
    def test_isMember():
        member = Member.objects.all().first()
        assert member.is_member()

        member.membership_end = datetime.now().date() - timedelta(days=2)
        assert not member.is_member()

    def test_isMember_equals_member_sonly(self):
        count_members_only = Member.objects.members_only().count()
        count_is_member = 0
        for member in Member.objects.all():
            if member.is_member():
                count_is_member += 1

        self.assertEqual(count_members_only, count_is_member)

    def test_money_to_pay(self):
        """Check the Money members have to pay"""

        '''star_set = Star.objects.all()
# The `iterator()` method ensures only a few rows are fetched from
# the database at a time, saving memory.
for star in star_set.iterator():'''
        # print(Member.objects.__dict__)
        for member in Member.objects.all():
            # test getMoneyToPay is multiple of FEE
            self.assertEqual(member.get_money_to_pay() % member.get_annual_fee(),
                             (member.get_annual_fee() - member.account_balance) % member.get_annual_fee())
            # print(member.chaos_number)
            # print(member.getName())
            # print(member.account_balance)
            # print(member.getMoneyToPay())
            # print('#' * 10)

        # self.assertEqual(lion.speak(), 'The lion says "roar"')
        # self.assertEqual(cat.speak(), 'The cat says "meow"')

    def test_billing_cycle(self):
        billed_response = api_views.billing_cycle(None)
        # self.assertEqual('HONORARY' in str(billed), False)
        print(BillingCycle.objects.all())
        self.assertContains(billed_response, 'HONORARY', False)

    def test_delayed_payment_mails(self):
        # create delayed payment mails
        api_views._mail_create(api_views.MAIL_CREATE_DELAYED_PAYMENT, 0)
        print(EmailToMember.objects.all())
        print(EmailToMember.objects.get(pk=len(EmailToMember.objects.all())).body)

        self.assertEqual('HONORARY' in str(EmailToMember.objects.all()), False)

    @staticmethod
    def test_delayed_payment_export():
        api_views.create_delayed_payment_export()

    def test_member_exit(self):
        api_views._member_exit(chaos_number=133742)
        m = Member.objects.get(chaos_number__exact=133742)
        self.assertIsNone(m.erfa)
        self.assertEqual(m.address_1, "")
        self.assertIsNone(m.address_2)
        self.assertIsNone(m.address_3)
        self.assertEqual(m.address_country, "")
        self.assertIsNone(m.fee_override)
        self.assertEqual(m.first_name, "")
        self.assertEqual(m.last_name, "")
        self.assertEqual(m.comment, "")
        self.assertEqual(m.membership_reduced, False)
        pass

    def test_member_save(self):
        m = Member.objects.first()
        m.address_1 = "foo"
        m.address_unknown = True
        m.save()
        m = Member.objects.first()
        self.assertEqual(m.address_unknown, True, 'i just saved it')

        m.last_name += " foo"
        m.save()
        self.assertEqual(m.address_unknown, True)
        m.address_1 += "20 bar"
        m.save()
        self.assertEqual(m.address_unknown, False)
        m = Member.objects.first()
        self.assertEqual(m.address_unknown, False)

        # TODO: make some kind REAL of test for EmailToMember.objects
        self.assertEqual(len(EmailToMember.objects.all()), 14)

        # print('### EmailToMember LEN: ', len(EmailToMember.objects.all()))
        # for mail in EmailToMember.objects.all():
        #     print(mail.)
        # print(EmailToMember.objects.all())

    def test_change_balance(self):
        # test modifications to balance
        m = Member.objects.first()
        m.set_balance_to(33)
        self.assertEqual(m.account_balance, 33)
        m = Member.objects.first()  # reload so checking changes vs logging match
        m.increase_balance_by(67)
        self.assertEqual(m.account_balance, 100)
        # test logging
        transaction_logs = BalanceTransactionLog.objects.filter(member=m)
        transaction_log = transaction_logs[len(transaction_logs) - 1]
        self.assertEqual(transaction_log.changed_value, 67)
        self.assertEqual(transaction_log.new_value, 100)

    def test_balance_logging(self):
        # force known balance
        m = Member.objects.first()
        m.set_balance_to(10)
        btl = m.balancetransactionlog_set.all()
        last_btl = list(btl).pop()
        # check that a log has been made
        self.assertGreater(len(btl), 0)
        # check it bares expected value
        self.assertEqual(last_btl.new_value, 10)


class EmailToMemberTestCase(TestCase):

    def setUp(self):
        self.member = Member(first_name='first', last_name='last', address_1='address', address_country='DE')
        self.member.save()

    def test_welcome_email(self):
        # creating a member should create a welcome email
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'Only one mail should be generated when creating a member.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_WELCOME,
                         'The first email a member receives should be the welcome mail.')

    def test_welcome_email_after_change(self):
        # a change should not change anything if a welcome mail is in queue
        self.member.address_2 = 'new place'
        self.member.save()
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'Even after a change on the member, the welcome mail should be the one and only.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_WELCOME,
                         'The first email a member receives should be the welcome mail.')

    def test_data_record_after_change(self):
        # multiple changes should only yield one data record mail
        self.member.emailtomember_set.all().delete()

        self.member.address_2 = 'another place'
        self.member.save()
        self.member.address_2 = 'yet another place'
        self.member.save()
        self.assertEqual(self.member.emailtomember_set.count(), 1,
                         'After a data change a data record email should be sent, but only one, even after multiple'
                         'changes.')
        self.assertEqual(self.member.emailtomember_set.first().email_type, EmailToMember.SEND_TYPE_DATA_RECORD,
                         'Mail should be of type data record.')


class EmailAddressTestCase(TestCase):

    def setUp(self):
        # One member is all we need
        self.member = Member(first_name='first', last_name='last', address_1='address', address_country='DE')
        self.member.save()

    def test_uniqueness(self):
        from django.db import IntegrityError
        email_address_1 = EmailAddress(member=self.member, email_address='a@example.com')
        email_address_2 = EmailAddress(member=self.member, email_address='a@example.com')
        email_address_1.save()
        with self.assertRaises(IntegrityError, msg='If this fails try deleting your db.sqlite3 and rebuilding it.'):
            email_address_2.save()

    def test_primary(self):
        def exactly_one_primary():
            self.assertEqual(EmailAddress.objects.filter(is_primary=True).count(), 1,
                             'There should be exactly one primary email address at any given time')

        email_address_1 = EmailAddress(member=self.member, email_address='a@example.com')
        email_address_1.save()
        self.assertTrue(email_address_1.is_primary,
                        'The first and only email address should be primary, even if not specified')

        email_address_2 = EmailAddress(member=self.member, email_address='b@example.com', gpg_key_id='0x12341234')
        email_address_2.save()
        self.assertFalse(email_address_2.is_primary, 'If not being requested to be primary it should not be granted')

        exactly_one_primary()

        email_address_3 = EmailAddress(member=self.member, email_address='c@example.com', is_primary=True)
        email_address_3.save()
        self.assertTrue(email_address_3.is_primary, 'If being requested to be primary it should be granted')

        exactly_one_primary()

        email_address_3.delete()
        email_address_2.refresh_from_db()
        self.assertTrue(email_address_2.is_primary,
                        'When needing a new primary email address gpg ones should be preferred')

        exactly_one_primary()

        email_address_1.delete()
        email_address_2.refresh_from_db()
        self.assertTrue(email_address_2.is_primary,
                        'Deleting a non-primary email address should not change any other email address')

        exactly_one_primary()

        # Well, deleting the last email address should just not throw any exceptions
        email_address_2.delete()


class StatisticsTestCase(TestCase):

    def setUp(self):
        # Create 9 members
        self.numberOfKinds = 2
        mock_members(self.numberOfKinds, list(range(1000)))
        self.erfa1 = Erfa.objects.create(short_name="ARD",
                                         long_name="Allerlei Richtige Dorfler",
                                         has_doppelmitgliedschaft=True)
        self.erfa2 = Erfa.objects.create(short_name="ZDF",
                                         long_name="Zwielichtige Dorf-Frickler",
                                         has_doppelmitgliedschaft=False)
        nr_of_members = Member.objects.count()
        for member_id, mem in enumerate(Member.objects.all()):
            if mem.membership_type == Member.MEMBERSHIP_TYPE_SUPPORTER:
                mem.erfa = self.erfa2
            elif member_id < (nr_of_members / 2):
                # 5 times
                mem.erfa = self.erfa1
            else:
                # 4 times
                mem.erfa = self.erfa2

            mem.save()

    def test_stats(self):
        result = api_views._erfa_statistics()
        print(result)
        self.assertEqual(result[self.erfa2.get_short_long_name()][
                         Member.MEMBERSHIP_TYPE_SUPPORTER], self.numberOfKinds)
        self.assertEqual(result[self.erfa2.get_short_long_name()][
                         Member.MEMBERSHIP_TYPE_HONORARY], 1)
        self.assertEqual(result[self.erfa1.get_short_long_name()][
                         Member.MEMBERSHIP_TYPE_MEMBER], 6)
        pass
