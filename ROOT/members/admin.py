from django.contrib import admin
from .models import Member, EmailAddress, Erfa, EmailToMember, ArchivedEmail, BillingCycle, BalanceTransactionLog
from ROOT.settings import DEBUG


@admin.register(EmailAddress)
class EmailAdmin(admin.ModelAdmin):
    readonly_fields = ('gpg_error',)


class EmailInline(admin.StackedInline):
    readonly_fields = ('gpg_error',)
    extra = 0
    model = EmailAddress


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    inlines = [
        EmailInline,
    ]

    list_display = ('chaos_number', 'get_name', 'get_address', 'is_member', 'is_active', 'fee_paid_until',
                    'account_balance', 'get_emails_string', 'comment')
    search_fields = ('chaos_number', 'first_name', 'last_name', 'address_1', 'address_2', 'address_3',
                     'emailaddress__email_address', '=address_country', 'comment')

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ["chaos_number",
                                "membership_end",
                                "fee_last_paid",
                                "fee_paid_until"
                                ]

        return super().change_view(request, object_id, form_url, extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        self.exclude = ["chaos_number",
                        "membership_end",
                        "fee_last_paid",
                        "fee_paid_until",
                        "fee_registration_pai"]

        return super().add_view(request, form_url, extra_context)

    def save_model(self, request, obj, form, change):
        if 'account_balance' in form.changed_data:
            increase_by = obj.account_balance - obj._initial_state['account_balance']
            obj.log_increased_balance(increased_by=increase_by,
                                      reason=BalanceTransactionLog.MANUAL_BOOKING,
                                      comment="balance set via member admin")
        super(MemberAdmin, self).save_model(request, obj, form, change)


@admin.register(Erfa)
class ErfaAdmin(admin.ModelAdmin):
    pass


@admin.register(EmailToMember)
class EmailToMemberAdmin(admin.ModelAdmin):
    # list_display = ('member', 'subject', 'body', 'created', 'ready_to_send', 'email_type', ')

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ['rendered_preview']
        return super().change_view(request, object_id, form_url, extra_context)


@admin.register(ArchivedEmail)
class EmailArchiveAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(BillingCycle)
class BillingCycleAdmin(admin.ModelAdmin):
    def has_change_permission(self, request, obj=None):
        if DEBUG:
            return True
        else:
            return False

    def has_add_permission(self, request, obj=None):
        if DEBUG:
            return True
        else:
            return False

    def has_delete_permission(self, request, obj=None):
        if DEBUG:
            return True
        else:
            return False


@admin.register(BalanceTransactionLog)
class BalanceTransactionLogAdmin(admin.ModelAdmin):
    readonly_fields = ('created_on', 'member', 'changed_value', 'new_value')
