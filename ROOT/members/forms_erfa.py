from django import forms
from .models import Erfa


class SimpleFileUploadForm(forms.Form):

    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )


class ErfaSelectForm(forms.Form):
    erfa = forms.ModelChoiceField(queryset=Erfa.objects.filter(has_doppelmitgliedschaft=True), empty_label="Alle auswählen", required=False)


class ErfaSelectFileUploadForm(SimpleFileUploadForm):

    erfa = forms.ModelChoiceField(queryset=Erfa.objects.filter(has_doppelmitgliedschaft=True), required=True)


class ErfaSelectFormAll(forms.Form):
    erfa = forms.ModelChoiceField(queryset=Erfa.objects.all(), empty_label="Alle auswählen", required=False)


class ErfaSelectFileUploadFormAll(SimpleFileUploadForm):

    erfa = forms.ModelChoiceField(queryset=Erfa.objects.all(), required=True)
