# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='balancetransactionlog',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 2, 13, 57, 27, 425304), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_override',
            field=models.IntegerField(blank=True, null=True, default=None),
        ),
    ]
