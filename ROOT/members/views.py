from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from members.models import Member, Erfa, EmailAddress
from ROOT.settings import EMPTY_ERFA_NAME
from django import forms
from datetime import datetime, date
from members.forms_members import MemberImportForm
from members.forms_search import SearchForm
from members.forms_revert_transaction import BalanceTransactionLogForm

from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext
from django.core.validators import validate_email
from members.countryfield import get_country_dict
from members.models import BalanceTransactionLog
from django.shortcuts import redirect

from .forms_erfa import ErfaSelectForm, ErfaSelectFileUploadForm, ErfaSelectFormAll,\
    ErfaSelectFileUploadFormAll, SimpleFileUploadForm
from .api_helper import ErfaAbgleich, CashPoint, Vereinstisch
from wsgiref.util import FileWrapper
import io
import csv
import os


def index(request):
    return render(request, "index_members.html", {})


def delete_emails(request):
    errors = []
    deleted = []

    from .forms_delete_emails import DeleteEmailsForm

    if request.method == "POST":
        if 'mailsfield' in request.POST:
            lines = request.POST['mailsfield'].replace("\r", '').split("\n")
            total_deletions = 0
            for email in lines:
                try:
                    validate_email(email)

                    email_addresses_to_delete = EmailAddress.objects.filter(email_address__iexact=email)
                    total_deletions += len(email_addresses_to_delete)

                    for email_address in email_addresses_to_delete:
                        # Not calling .delete() on the Queryset, because then delete() of EmailAddress would not be
                        # called
                        email_address.delete()
                        deleted.append('deleted email {} for {}'.format(email, email_address.member.get_name()))

                except forms.ValidationError:
                    errors.append('ValidationError: "{}"'.format(email))

            if total_deletions == 0:
                errors.append('NOTHING FOUND')
        else:
            # TODO: fileupload
            # form = DeleteEmailsForm(request.POST, request.FILES)
            # print(form.__dict__)
            # print(request.POST['mailsfield'])
            # if form.is_valid():
            #     uploaded_file = request.FILES['docfile']
            #     data = uploaded_file.read().decode("latin-1")  # windows-1252
            pass
    form = DeleteEmailsForm()
    return render(request, 'delete_emails.html',
                  {'form': form, 'errors': errors, 'deleted': deleted}
                  )  # , {'field': 'value'}


def add_member(request):
    from .forms_members import MemberAddForm, EmailFormset

    new_chaos_number = 0
    if request.method == 'POST':
        member_form = MemberAddForm(prefix='member', data=request.POST)
        email_formset = EmailFormset(data=request.POST)
        if member_form.is_valid():
            new_member = member_form.save(commit=False)
            new_member.chaos_number = -1
            email_formset = EmailFormset(data=request.POST, instance=new_member)
            if email_formset.is_valid():
                new_member.chaos_number = None
                new_member.save()
                email_formset.save()
                new_chaos_number = new_member.chaos_number

                # prepare new empty member form
                email_formset = EmailFormset()
                member_form = MemberAddForm(prefix='member')
    else:
        email_formset = EmailFormset()
        member_form = MemberAddForm(prefix='member')
    return render(request, 'add_member.html', {'member_form': member_form,
                                               'email_formset': email_formset,
                                               'new_chaos_number': new_chaos_number})


def _combine_values(all_fields_ar):
    final_string = ' '.join(all_fields_ar)
    return final_string.strip()


def _bool_converter(value):
    value = value.lower()
    if value == 'true' or value == 'wahr' or value == 'war':
        return True
    elif value == 'false' or value == 'falsch':
        return False
    else:
        print('WARNING: VALUE IS NOT BOOL: "' + value + '"')
        return False


def _date_re_format(bad_date):
    dot_count = bad_date.count('.')
    # print('BAD DATE:',badDate)
    if dot_count == 2:
        # print('BAD DATE: "'+badDate+'"')
        new_date = datetime.strptime(bad_date, '%d.%m.%y')  # .strftime('%Y-%m-%d')
        # print('new date: '+str(new_date))
        return new_date.date()
    elif dot_count > 0 or bad_date != '':
        print('WARNING: BAD DATE IS STRANGE: "' + bad_date + '"')

    return None

_erfaWithDoppelmitgliedschaft = ['HHV', 'ACV', 'C4', 'ZHV', 'PBV', 'DUV', 'XMZ', 'MAV', 'CKL']

_erfaLongName = {
    'HH': 'Hamburg',
    'B': 'Berlin',
    'K': 'Köln',
    'ULM': 'Ulm',
    'DUE': 'Düsseldorf',
    'KAR': 'Karlsruhe',
    'HAN': 'Hannover',
    'MUC': 'München',
    'ERL': 'Erlangen',
    'KAS': 'Kassel',
    'AC': 'Aachen',
    'W': 'Wien',
    'DA': 'Darmstadt',
    'DD': 'Dresden',
    'ZUE': 'Zürich',
    'MZ': 'Mainz',
    'PB': 'Paderborn',
    'HB': 'Bremen',
    'MA': 'Mannheim',
    'FFM': 'Frankfurt a.M.',
    'GOE': 'Göttingen',
    'FR': 'Freiburg',
    'E': 'Essen',
    'S': 'Stuttgart',
    'KL': 'Kaiserslautern',
    'BA': 'Bamberg',

    # Vereine ohne Erfa-Rueckerstattung
    'HHV': 'Hamburg e.V.',
    'ACV': 'Aachen e.V.',
    'C4': 'Köln C4 e.V.',
    'ZHV': 'Zürich Verein',
    'PBV': 'Paderborn e.V.',
    'DUV': 'Düsseldorf e.V.',
    'XMZ': 'Mainz e.V.',
    'MAV': 'Mannheim e.V.',
    'CKL': 'Kaiserslautern e.V.'
    }

_erfaFixDict = {
    'HAM': 'HH',
    'BLN': 'B',
    'EL': 'ERL',
    'KLN': 'K',
    'UML': 'ULM',
    'UL': 'ULM',
    'D': 'DUE',
    'DUS': 'DUE',
    'DU': 'DUE',
    'FRA': 'FFM',
    'F': 'FFM',
    'KA': 'KAR',
    'H': 'HAN',
    'M': 'MUC',
    'KS': 'KAS',
    'AA': 'AC',
    'A': 'AC',
    'AAC': 'AC',
    'WIE': 'W',
    'DRE': 'DD',
    'C3D': 'DD',
    'Z': 'ZUE',
    'ZH': 'ZUE',
    'BRE': 'HB',
    'FRE': 'FR',
    'CZH': 'ZHV',
    'CHZ': 'ZHV',
    'BR': 'HB',
    'ST': 'S',
    'AB': 'BA',
    'ER': 'ERL',
    'HL': EMPTY_ERFA_NAME,
    'BIE': EMPTY_ERFA_NAME,
    'CH': EMPTY_ERFA_NAME,
    'ALI': EMPTY_ERFA_NAME,
    'MG': EMPTY_ERFA_NAME,
    'L': EMPTY_ERFA_NAME,
    'OS': EMPTY_ERFA_NAME,
    'N': EMPTY_ERFA_NAME,
    'RUH': EMPTY_ERFA_NAME,
    'BO': EMPTY_ERFA_NAME,
    'TR': EMPTY_ERFA_NAME,
    'RP': EMPTY_ERFA_NAME,
    'HEI': EMPTY_ERFA_NAME,
    'MUE': EMPTY_ERFA_NAME,
    'BAS': EMPTY_ERFA_NAME,
    'DO': EMPTY_ERFA_NAME,
    'MS': EMPTY_ERFA_NAME,
    'R': EMPTY_ERFA_NAME,
    'MRM': EMPTY_ERFA_NAME,
    'GIE': EMPTY_ERFA_NAME,
    'HEF': EMPTY_ERFA_NAME,
    'BSL': EMPTY_ERFA_NAME,
    'HD': EMPTY_ERFA_NAME,
    '': EMPTY_ERFA_NAME
    }

_countryDict = get_country_dict()
_countryFixDict = {
    'D 6': 'DE', 'D': 'DE', 'DEU': 'DE', 'DD': 'DE',
    'UK': 'GB',
    'US9': 'US', 'USA': 'US',
    'C': 'CH',
    'CHI': 'CN',
    'ITA': 'IT', 'I': 'IT',
    'JOR': 'JO',
    'SGP': 'SG',
    'ARG': 'AR',
    'NOR': 'NO',
    'F': 'FR',
    'IRL': 'IE',
    'AUS': 'AU',
    'N': 'NO',
    'RUS': 'RU',
    'A': 'AT', 'N': 'AT',
    'H': 'HU',
    'B': 'BE',
    'FIN': 'FI',
    'IND': 'ID',
    'E': 'ES',
    'L': 'LU',
    'P': 'PL'
    # CHI = CHILE (	CL) ?
    # 'AUS':'AT' # UNSURE!
    # A austria
    # B belgium / brasilien
    # F france
    # P poland
    # L lichtenstein
    # I italy
    # H hungary
}


def _country_fix(country):
    country = country.upper()
    if country == '':
        return ''
    elif country in _countryDict:
        return country
    elif country in _countryFixDict:
        return _countryFixDict[country]
    else:
        print('WARNING: BAD COUNTRY: "' + country + '"')
        return country


def run_import_members(request):
    from datenschleuder.models import Subscriber as datenschleuderSubscriber

    def is_datenschleuder(row):
        return (row['RST,N,3,0'].isnumeric() and int(row['RST,N,3,0']) < 999) or _bool_converter(row['PRV,L']) or\
               _bool_converter(row['AAA,L']) or _bool_converter(row['FRE,L'])

    def add_ds_member(row):
        # datenschleuder MTG,L, False / True <- Darauf kann man sich nicht verlassen
        # row['RST,N,3,0' # Datenschleuder max-issue
        new_datenschleuder_subscriber = datenschleuderSubscriber()

        new_datenschleuder_subscriber.datenschleuder_number = int(row['NR,N,5,0'])

        new_datenschleuder_subscriber.subscriber_source = datenschleuderSubscriber.IMPORT_INITIAL

        new_datenschleuder_subscriber.first_name = row['VNM,C,10']
        new_datenschleuder_subscriber.last_name = row['NME,C,20']

        new_datenschleuder_subscriber.address_1 = _combine_values([
                                row['ORG,C,30'],
                                row['STR,C,30'],
                            ])
        new_datenschleuder_subscriber.address_2 = _combine_values([
                                row['PLZ,N,5,0'],
                                row['ORT,C,40'],
                                # row['BND,C,30'],
                            ])
        new_datenschleuder_subscriber.address_3 = ''
        new_datenschleuder_subscriber.address_country = _country_fix(row['LND,C,3'])

        if (not new_datenschleuder_subscriber.address_1.strip() and
                not new_datenschleuder_subscriber.address_2.strip()) or _bool_converter(row['UBK,L']):
            new_datenschleuder_subscriber.address_unknown = True

        if not _bool_converter(row['AKT,L']):
            new_datenschleuder_subscriber.max_datenschleuder_issue = 0
        else:
            new_datenschleuder_subscriber.max_datenschleuder_issue = int(row['RST,N,3,0'])
        new_datenschleuder_subscriber.is_endless =\
            _bool_converter(row['PRV,L']) or _bool_converter(row['AAA,L']) or _bool_converter(row['FRE,L'])

        # Fehler in dem Skript zum Eintragen der DS-Abos hat den : durch ein ; ersetzt
        if row['KOM,C,60'].startswith('https;'):
            new_datenschleuder_subscriber.rt_link = row['KOM,C,60'].replace(';', ':')

        new_datenschleuder_subscriber.save()
        return new_datenschleuder_subscriber

    def import_add_member(line_num, row):
        new_member = Member()
        # This instance only attribute is being evaluated by the save signal and ensures to not send a welcome email
        new_member._imported = True

        new_member.chaos_number = row['NR,N,5,0']

        if _bool_converter(row['EZE,L']):
            new_member.membership_type = Member.MEMBERSHIP_TYPE_SUPPORTER
        else:
            new_member.membership_type = Member.MEMBERSHIP_TYPE_MEMBER

        new_member.membership_reduced = _bool_converter(row['ERM,L'])

        new_member.first_name = row['VNM,C,10']
        new_member.last_name = row['NME,C,20']

        '''
        ORG,C,30
        Organisation (Teil der Anschrift), 30 Zeichen

        STR,C,30
        Straße

        PLZ,N,5,0
        Postleitzahl, 5-stellige Zahl, per Default 0

        ORT,C,40

        BND,C,30d
        Bundesland, eigentlich ungenutzt, steht aber in Einträgen <= 2043 oder
        so was drin.
        '''
        new_member.address_1 = _combine_values([
                                row['ORG,C,30'],
                                row['STR,C,30'],
                            ])
        new_member.address_2 = _combine_values([
                                row['PLZ,N,5,0'],
                                row['ORT,C,40'],
                                # row['BND,C,30'],
                            ])
        new_member.address_3 = ''

        if (not new_member.address_1.strip() and not new_member.address_2.strip()) or _bool_converter(row['UBK,L']):
            new_member.address_unknown = True

        '''
        LND,C,3
        Land
        '''
        # print('country:'+row['LND,C,3'])
        new_member.address_country = _country_fix(row['LND,C,3'])

        new_member.comment = row['KOM,C,60']

        erfa_str = row['ERK,C,3'].upper().strip()

        if erfa_str in _erfaFixDict:
            erfa_str = _erfaFixDict[erfa_str]

        try:
            erfa = Erfa.objects.get(short_name=erfa_str)
        except Erfa.DoesNotExist:
            print('New ERFA: "' + erfa_str + '"')
            erfa = Erfa()
            erfa.short_name = erfa_str
            if erfa_str in _erfaLongName:
                erfa.long_name = _erfaLongName[erfa_str]
            else:
                erfa.long_name = erfa_str
            erfa.has_doppelmitgliedschaft = erfa_str in _erfaWithDoppelmitgliedschaft
            erfa.save()

        if erfa is not None:
            new_member.erfa = erfa

        email_str = row['EML,C,50'].strip()
        if email_str != '':
            emails_ar = email_str.split(',')
            first_mail = True
            for mailStr in emails_ar:
                mail = EmailAddress()
                mail.member = new_member
                mail.email_address = mailStr.strip().lower()
                mail.gpg_key_id = row['PGPKEY,C,42'].strip()
                mail.is_primary = first_mail
                first_mail = False
                try:
                    # This instance only attribute is being evaluated by the save signal and ensures to not send a
                    # welcome email
                    mail._imported = True
                    mail.save()
                except Exception as exception:
                    errors.append('Error at line ' + str(line_num) + ' @ mail "' + mailStr + '" Message: ' +
                                  str(exception) + ' LINE: ' + str(row))

        '''
        LZA,D
        Letzte Zahlung am, Datum

        EIN,D
        Eintrittsdatum

        BZB,D
        Bezahlt bis

        AAM,D
        Ausgetreten am
        '''

        new_member.last_update = _date_re_format(row['LUP,D'])

        membership_start = _date_re_format(row['EIN,D'])
        if membership_start is None:
            membership_start = datetime.strptime('12.09.1981', '%d.%m.%Y').date()  # datetime.date(1981, 12, 9)#
            # print('BZB,D:', row['BZB,D'])
        new_member.membership_start = membership_start

        last_paid = _date_re_format(row['LZA,D'])
        if last_paid is None:
            new_member.fee_last_paid = membership_start
        else:
            new_member.fee_last_paid = last_paid

        paid_until = _date_re_format(row['BZB,D'])
        if paid_until is None:
            new_member.fee_paid_until = membership_start
        else:
            new_member.fee_paid_until = paid_until

        new_member.membership_end = _date_re_format(row['AAM,D'])
        # temporarily inactive member
        if new_member.membership_end is None and not _bool_converter(row['AKT,L']):
            new_member.is_active = False

        # exited member, delete all data
        if new_member.membership_end is not None or 'AUTO: geloescht wg. Zahlungsmangel' in new_member.comment:
            new_member.exit()
        else:
            new_member.save()
        return new_member

    # Handle file upload
    uploaded_file = ''
    errors = []
    imported = []
    if request.method == 'POST':
        form = MemberImportForm(request.POST, request.FILES)
        if form.is_valid():

            uploaded_file = request.FILES['docfile']
            # DONE: iso-8859-1 / latin-1
            data = uploaded_file.read().decode("latin-1")  # windows-1252

            # data = uploaded_file.read()
            f = io.StringIO(data)
            csv_data = csv.DictReader(f, delimiter="\t")

            for linenum, csv_row in enumerate(csv_data):
                try:
                    if is_datenschleuder(csv_row):
                        imported.append(str(add_ds_member(csv_row)))
                    else:
                        imported.append(str(import_add_member(linenum, csv_row)))
                except Exception as ex:
                    errors.append(
                        'Error at line ' + str(linenum) +
                        ' Message: ' + str(ex) +
                        ' LINE: "' + str(csv_row) + '"')

    form = MemberImportForm()  # A empty, unbound form
    response = render_to_response(
            'import_members.html',
            {'form': form, 'uploaded_file': uploaded_file, 'errors': errors, 'imported': imported},
            context_instance=RequestContext(request)
        )
    return response


def search_member_db(request):
    # TODO: if request.method == 'GET':
    # Show complete page including search
    # TODO: if request.method == 'POST':
    # Redirect to search API
    if request.method == 'POST':
        return HttpResponse("OK")
    else:
        form = SearchForm()
        return render(request, 'search_form.html', {'form': form})
    pass


def monthly_statistics(request):
    context = {'statistics_type': 'monthly',
               'month': date.today().month
               }
    return render(request, 'statistics.html', context)


def general_statistics(request):
    del request
    # Put Zip analysis, country analysis a.s.o. here
    pass


def erfaabgleich(request):
    return render(request, 'erfaabgleich.html')


def erfaabgleich_export(request, all_erfas=False):
    if all_erfas:
        form_class = ErfaSelectFormAll
    else:
        form_class = ErfaSelectForm
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            if form.data['erfa']:
                export_list = Erfa.objects.filter(pk=form.data['erfa'][0])
            else:
                export_list = Erfa.objects.all()
            files = []
            os.system("mkdir -p /tmp/exports/")
            if os.path.exists('/tmp/exports/erfaabgleich.zip'):
                os.unlink('/tmp/exports/erfaabgleich.zip')
            for erfa in export_list:
                filename = "{}.csv".format(erfa.short_name)
                files.append(filename)
                filename = "/tmp/exports/" + filename
                print(erfa, filename)
                csv_export = ErfaAbgleich(filename, erfa.member_set.all())
                csv_export.do_export()
            os.system("cd /tmp/exports/ && zip /tmp/exports/erfaabgleich.zip {}".format(" ".join(files)))
            response = HttpResponse(FileWrapper(open('/tmp/exports/erfaabgleich.zip', 'rb')),
                                    content_type='application/zip')
            response['Content-Disposition'] = 'attachment; filename=erfaabgleich.zip'
            return response
    else:
        form = form_class()
    return render(request, 'erfaabgleich_export.html', {'form': form, 'h1': "Erfa Abgleich Export"})


def erfaabgleich_import(request, all_erfas=False):
    import_logs = None
    warnings = None
    if all_erfas:
        form_class = ErfaSelectFileUploadFormAll
    else:
        form_class = ErfaSelectFileUploadForm
    if request.method == "POST":
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            erfa = Erfa.objects.get(pk=form.data['erfa'][0])
            uploaded_file = request.FILES['docfile']
            data = uploaded_file.read().decode("utf8")
            f = io.StringIO(data)
            csv_import = ErfaAbgleich(f, erfa)
            csv_import.do_import()
            import_logs = csv_import.logs
            warnings = csv_import.warnings
    else:
        form = form_class()
    return render(request, 'erfaabgleich_import.html', {'form': form,
                                                        'logs': import_logs,
                                                        'warnings': warnings,
                                                        'h1': 'Erfa Abgleich Import'})


def cashpoint(request):
    del request
    pass


# def cashpoint_import(request):
#     from .api_helper import CashPoint

#     errors = {}
#     imported = {}
#     uploadedFile = ''

#     if request.method == 'POST':
#         form = CashPointImportForm(request.POST, request.FILES)
#         if form.is_valid():
#             uploadedFile = request.FILES['docfile']
#             data = uploadedFile.read().decode("utf8")  # windows-1252
#             print(data)
#             #csv = CashPoint(fullPath, export_list)
#             #csv.do_export()
#             # data = uploadedFile.read()
#             #f = io.StringIO(data)

#     form = MemberImportForm() # A empty, unbound form
#     response = render_to_response(
#             'cashpoint_import.html',
#             { 'form': form, 'uploadedFile': uploadedFile, 'errors': errors, 'imported': imported },
#             context_instance=RequestContext(request)
#         )
#     return response

def cashpoint_export(request):  # , all_erfas=False):
    if request.method == "POST":
        files = []
        os.system("mkdir -p /tmp/exports/")
        if os.path.exists('/tmp/exports/cashpoint.zip'):
            os.unlink('/tmp/exports/cashpoint.zip')
        filename = "cashpoint.csv"
        files.append(filename)
        filename = "/tmp/exports/" + filename
        csv_export = CashPoint(filename, Member.objects.members_only())
        csv_export.do_export()
        os.system("cd /tmp/exports/ && zip /tmp/exports/cashpoint.zip {}".format(" ".join(files)))
        response = HttpResponse(FileWrapper(open('/tmp/exports/cashpoint.zip', 'rb')), content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=cashpoint.zip'
        return response
    else:
        form = ''
    return render(request, 'erfaabgleich_export.html', {'form': form, 'h1': 'Cash Point export'})


def vereinstisch_import(request):  # , all_erfas=False):
    import_logs = None
    warnings = None
    form_class = SimpleFileUploadForm
    if request.method == "POST":
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            uploaded_file = request.FILES['docfile']
            data = uploaded_file.read().decode("utf8")
            f = io.StringIO(data)
            csv_import = Vereinstisch(f)
            csv_import.do_import()
            import_logs = csv_import.logs
            warnings = csv_import.warnings
    else:
        form = form_class()
    return render(request, 'erfaabgleich_import.html', {'form': form,
                                                        'logs': import_logs,
                                                        'warnings': warnings,
                                                        'h1': 'Verinstisch Import'})


def show_transaction_log(request):
    qs = BalanceTransactionLog.objects.order_by('-created_on')
    # context = {"transaction_logs":qs}
    paginator = Paginator(qs, 10)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)

    page_query = qs.filter(id__in=[obj.id for obj in objects])
    context = {'objects': objects, 'transaction_logs': page_query}

    return render(request=request, context=context, template_name='show_transaction_logs.html')


def member_address_unknown(request):
    return render(request, 'address_unknown.html')


def anti_transaction(request, pk):
    if request.method == "POST":
        form = BalanceTransactionLogForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            d_initial = BalanceTransactionLog.objects.filter(pk=pk).values()[0]
            v = d_initial["changed_value"] * -1
            m = Member.objects.get(pk=d_initial["member_id"])
            m.increase_balance_by(v, comment="Reverted from:{} (was BalanceLog: {})".format(d_initial["comment"], pk))
            return redirect(show_transaction_log)  # Redirect after POST
    else:
        inst = BalanceTransactionLog.objects.get(pk=pk)
        form = BalanceTransactionLogForm(instance=inst)

        form.fields['member'].widget = forms.HiddenInput()
        form.fields['changed_value'].widget = forms.HiddenInput()
        form.fields['new_value'].widget = forms.HiddenInput()
        form.fields['comment'].widget = forms.HiddenInput()

    return render(request, 'confirm_revert.html', {
        'form': form,
    })
