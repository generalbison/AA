from django import forms

from .models import Member
from .models import EmailAddress

class DeleteEmailsForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )
    mailsfield = forms.CharField(widget=forms.Textarea)
    #mailsfield = forms.TextInput(
    #    label='emails',
    #    help_text='enter emails, one per line'
    #)